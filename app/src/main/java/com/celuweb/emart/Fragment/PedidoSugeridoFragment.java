package com.celuweb.emart.Fragment;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.celuweb.emart.Adapter.ListaPedidoSugeridoAdapter;
import com.celuweb.emart.DataController.CarritoDataController;
import com.celuweb.emart.DataController.DataController;
import com.celuweb.emart.DataObject.PedidoDetalle;
import com.celuweb.emart.DataObject.PedidoSugerido;
import com.celuweb.emart.DataObject.Producto;
import com.celuweb.emart.Emart.CatalogoActivity;
import com.celuweb.emart.Networking.APIs;
import com.celuweb.emart.Networking.Networking;
import com.celuweb.emart.R;
import com.celuweb.emart.Util.ProgressView;
import com.celuweb.emart.Util.ProgressView2;
import com.celuweb.emart.Util.Util;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class PedidoSugeridoFragment extends Fragment implements ListaPedidoSugeridoAdapter.IAdapterListener {
    private ArrayList<PedidoSugerido> pedidoSugerido;
    private RecyclerView recyclerSugerido;
    private RecyclerView recyclerPedido;
    private ListaPedidoSugeridoAdapter adapterSugerido, adapterPedido;


    public PedidoSugeridoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pedido_sugerido, container, false);
        initComponents(view);
        ProgressView.getInstance().Show(PedidoSugeridoFragment.this.getActivity(), new ProgressView.ICallback() {
            @Override
            public void run() {
                consultarPedidoSugerido();
            }
        });

        return view;
    }


    public void initComponents(View view) {
        recyclerSugerido = view.findViewById(R.id.recycler_pedsug);
        recyclerPedido = view.findViewById(R.id.recycler_pedido);
        recyclerSugerido.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerPedido.setLayoutManager(new LinearLayoutManager(getContext()));
        ((CatalogoActivity) this.getActivity()).getSupportActionBar().setTitle("Pedido Sugerido");
    }

    @Override
    public void onResume() {
        super.onResume();
        ((CatalogoActivity) this.getActivity()).getSupportActionBar().setTitle("Pedido Sugerido");
    }

    public void consultarPedidoSugerido() {

        if (!Util.checkInternet(PedidoSugeridoFragment.this.getActivity())) {
            Util.showDialog(PedidoSugeridoFragment.this.getActivity(), "Alerta", "No Hay conexión a internet", "Aceptar", "", true, false, null, null);
            ProgressView.getInstance().Dismiss();
            return;
        }


        String params = "?origen=" + DataController.getInstance().getProveedor().getCodigo_empresa()
                + "&cliente=" + DataController.getInstance().getProveedor().getCodigo();


        String url = Util.getUrl(params, DataController.getInstance().getProveedor(), APIs.SUGERIDO_END);

        // String url = "http://66.33.94.211/api/sugerido?origen=luker&cliente=2000349861";

        Networking.get(url, new Networking.ICallback() {
            @Override
            public void onFail(String error) {
                ProgressView.getInstance().Dismiss();
                Util.showDialog(PedidoSugeridoFragment.this.getActivity(), "Alerta", "No se pudo descargar la lista de productos", "Aceptar", "", true, false, null, null);
            }

            @Override
            public void onSuccess(String response) {
                ProgressView.getInstance().Dismiss();
                JSONObject jsonObject = null;
                try {

                    JSONArray jsonArray = new JSONArray(response);

                    Type arrayType = new TypeToken<ArrayList<PedidoSugerido>>() {
                    }.getType();

                    pedidoSugerido = new Gson().fromJson(jsonArray.toString(), arrayType);

                    //TODO INICIAR ADAPTERS Y LOS RECYCLERS
                    initRecycler(pedidoSugerido);

                } catch (JSONException e) {
                    ProgressView.getInstance().Dismiss();
                    e.printStackTrace();
                }

            }
        });

    }

    public void initRecycler(ArrayList<PedidoSugerido> pedidoSugerido) {
        ArrayList<Producto> listaPedido = CarritoDataController.getInstance().productos;
        ArrayList<PedidoSugerido> datosPedido = new ArrayList<>();
        ArrayList<PedidoSugerido> datosSugerido = new ArrayList<>();
        boolean flag = false;

        if (listaPedido != null) {
            for (int i = 0; i < pedidoSugerido.size(); i++) {
                flag = false;
                for (int j = 0; j < listaPedido.size(); j++) {
                    if (pedidoSugerido.get(i).getCodigo().equals(listaPedido.get(j).getCodigo())) {
                        datosPedido.add(pedidoSugerido.get(i));
                        datosPedido.get(datosPedido.size() - 1).setCantidadPedido(listaPedido.get(j).getCantidad());
                        datosPedido.get(datosPedido.size() - 1).cheked = true;
                        flag = true;
                        break;
                    }
                }
                if (flag == false) {
                    datosSugerido.add(pedidoSugerido.get(i));
                }
            }
        } else {
            datosSugerido = pedidoSugerido;
        }

        adapterPedido = new ListaPedidoSugeridoAdapter(datosPedido, this);
        recyclerPedido.setAdapter(adapterPedido);
        adapterSugerido = new ListaPedidoSugeridoAdapter(datosSugerido, this);
        recyclerSugerido.setAdapter(adapterSugerido);

    }

    @Override
    public void onMessageRowClicked(int position, boolean isChecked, int cantidad) {
        ArrayList<PedidoSugerido> datosSugerido = adapterSugerido.getListaDatos();
        ArrayList<PedidoSugerido> datosPedido = adapterPedido.getListaDatos();

        if (isChecked) {
            datosPedido.add(datosSugerido.get(position));
            datosPedido.get(datosPedido.size() - 1).setCantidadPedido(cantidad);
            datosSugerido.remove(position);
            datosPedido = checkList(datosPedido);
            adapterSugerido.actualizarAdapter(datosSugerido);
            adapterPedido.actualizarAdapter(datosPedido);
            //Agregando el pedido
            Producto item = new Producto();
            item.setNombre(datosPedido.get(datosPedido.size() - 1).getNombre());
            item.setCodigo(datosPedido.get(datosPedido.size() - 1).getCodigo());
            item.setPrecio(datosPedido.get(datosPedido.size() - 1).getPrecio());
            item.setCantidad(cantidad);
            item.setIva(datosPedido.get(position).getIva());
            CarritoDataController.getInstance().addProducto(item);
            int n = CarritoDataController.getInstance().getCantidadCarrito();
            ((CatalogoActivity) getActivity()).mostrarCantidad(n);
            //_______________________________

        } else {
            datosSugerido.add(0, datosPedido.get(position));
            datosPedido.remove(position);
            datosSugerido = unCheckList(datosSugerido);
            adapterSugerido.actualizarAdapter(datosSugerido);
            adapterPedido.actualizarAdapter(datosPedido);
            //Eliminar el pedido
            Producto item = new Producto();
            item.setNombre(datosSugerido.get(0).getNombre());
            item.setCodigo(datosSugerido.get(0).getCodigo());
            item.setPrecio(datosSugerido.get(0).getPrecio());
            item.setCantidad(cantidad);
            CarritoDataController.getInstance().removeProducto(item);
            int n = CarritoDataController.getInstance().getCantidadCarrito();
            ((CatalogoActivity) getActivity()).mostrarCantidad(n);
            //______________________________
        }


    }


    public ArrayList<PedidoSugerido> unCheckList(ArrayList<PedidoSugerido> datosSugerido) {

        for (int i = 0; i < datosSugerido.size(); i++) {
            datosSugerido.get(i).cheked = false;
        }
        return datosSugerido;
    }

    public ArrayList<PedidoSugerido> checkList(ArrayList<PedidoSugerido> datosPedido) {
        for (int i = 0; i < datosPedido.size(); i++) {
            datosPedido.get(i).cheked = true;
        }
        return datosPedido;
    }

    @Override
    public void onTextViewChange(int position, boolean isChecked, PedidoSugerido item) {

    }
}
