package com.celuweb.emart.Fragment;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.celuweb.emart.Adapter.BuscarAdapter;
import com.celuweb.emart.DataController.CarritoDataController;
import com.celuweb.emart.DataController.DataController;
import com.celuweb.emart.DataObject.ConsultaProductos;
import com.celuweb.emart.DataObject.Producto;
import com.celuweb.emart.Emart.CatalogoActivity;
import com.celuweb.emart.Networking.APIs;
import com.celuweb.emart.Networking.Networking;
import com.celuweb.emart.R;
import com.celuweb.emart.Util.ProgressView;
import com.celuweb.emart.Util.Util;
import com.google.gson.Gson;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class BuscarFragment extends Fragment implements View.OnClickListener{
    private EditText txtBusqueda;
    private ImageView btnBuscar, btnLimpiar, btnscan;
    private RecyclerView recyclerView;
    private BuscarAdapter adapter;
    private ArrayList<Producto> productos = new ArrayList<>();

    public BuscarFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_buscar, container, false);
        init(view);
        return view;
    }

    public void init(View view) {
        txtBusqueda = view.findViewById(R.id.txt_busqueda);
        btnBuscar = view.findViewById(R.id.btn_buscar);
        btnLimpiar = view.findViewById(R.id.btn_limpiar);
        btnscan = view.findViewById(R.id.btn_scan);
        recyclerView = view.findViewById(R.id.listaRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(BuscarFragment.this.getActivity()));
        btnBuscar.setOnClickListener(this);
        btnLimpiar.setOnClickListener(this);
        btnscan.setOnClickListener(this);


        txtBusqueda.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    consultarProductos(txtBusqueda.getText().toString());
                    return true;
                }
                return false;
            }
        });

        adapter = new BuscarAdapter(productos, this);
        recyclerView.setAdapter(adapter);
        ((CatalogoActivity) this.getActivity()).getSupportActionBar().setTitle("Buscar Producto");
    }


    private void consultarProductos(String busqueda) {
        String origen = DataController.getInstance().getProveedor().getCodigo_empresa();
        String cliente = DataController.getInstance().getProveedor().getCodigo();
        int page = 1;

        String params = "?origen=" + origen + "&cliente=" + cliente + "&page=" + page + "&search=" + busqueda;
        String url = Util.getUrl(params, DataController.getInstance().getProveedor(), APIs.BUSCAR_END);

        ProgressView.getInstance().Show(getActivity(), new ProgressView.ICallback() {
            @Override
            public void run() {
                Networking.get(url, new Networking.ICallback() {
                    @Override
                    public void onFail(String error) {
                        ProgressView.getInstance().Dismiss();
                    }

                    @Override
                    public void onSuccess(String response) {
                        ProgressView.getInstance().Dismiss();
                        ConsultaProductos consultaProductos = new Gson().fromJson(response, ConsultaProductos.class);
                        actualizarProductos(consultaProductos.getResult());
                        btnLimpiar.setVisibility(View.VISIBLE);
                    }
                });
            }
        });
    }


    private void actualizarProductos(ArrayList<Producto> productos) {
        this.productos = productos;
        adapter.productos = productos;
        adapter.notifyDataSetChanged();
    }

    public void actualizarProductoLista(Producto producto) {
        int n = CarritoDataController.getInstance().getCantidadCarrito();
        ((CatalogoActivity) getActivity()).mostrarCantidad(n);
        for (Producto p : productos) {
            if (p.getCodigo().equals(producto.getCodigo())) {
                p.setCantidad(producto.getCantidad());
                return;
            }
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onResume() {
        super.onResume();
        ((CatalogoActivity) this.getActivity()).getSupportActionBar().setTitle("Buscar Producto");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_buscar:

                try {
                    consultarProductos(URLEncoder.encode(txtBusqueda.getText().toString(), "UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.btn_limpiar:
                //TODO LIMPIAR LISTA
                productos = new ArrayList<>();
                adapter.productos = productos;
                adapter.notifyDataSetChanged();
                txtBusqueda.setText("");
                btnLimpiar.setVisibility(View.GONE);
                break;

            case R.id.btn_scan:
                escanearCodigoBarras();
                break;
        }
    }

    private void escanearCodigoBarras(){
        IntentIntegrator intentIntegrator = IntentIntegrator.forSupportFragment(BuscarFragment.this);
        intentIntegrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
        intentIntegrator.setPrompt("Escanear código de barras");
        intentIntegrator.setCameraId(0);
        intentIntegrator.setBeepEnabled(false);
        intentIntegrator.initiateScan();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null)
            if (result.getContents() != null) {
                txtBusqueda.setText(result.getContents());
                consultarProductos(result.getContents());
            } else {
                Util.showDialog(BuscarFragment.this.getActivity(), "Alerta", "No se pudo escanear el código de barras", "Aceptar", "", true, false, null, null);
            }
    }
}
