package com.celuweb.emart.Fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.celuweb.emart.Adapter.CarritoAdapter;
import com.celuweb.emart.DataController.CarritoDataController;
import com.celuweb.emart.DataController.DataController;
import com.celuweb.emart.DataObject.ListaDetalle;
import com.celuweb.emart.DataObject.Pedido;
import com.celuweb.emart.DataObject.Producto;
import com.celuweb.emart.Emart.CatalogoActivity;
import com.celuweb.emart.Emart.MainActivity;
import com.celuweb.emart.Networking.APIs;
import com.celuweb.emart.Networking.Networking;
import com.celuweb.emart.R;
import com.celuweb.emart.Util.ProgressView2;
import com.celuweb.emart.Util.Util;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.Callable;


public class CarritoFragment extends Fragment implements CarritoAdapter.IAdapterListener, View.OnClickListener {
    private RecyclerView recyclerView;
    private TextView lblTotal, lblSubTotal, lblTotalIva;
    private Button btnCancelar, btnPedido;
    private ArrayList<Producto> detallePedido;
    private CarritoAdapter adapter;
    private String observaciones;
    private View view;
    private EditText txtObservaciones;
    private double totalPedido;

    public CarritoFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_carrito, container, false);
        initComponents(view);
        calcularTotaltes();
        return view;
    }

    public void initComponents(View view) {
        ((CatalogoActivity) this.getActivity()).getSupportActionBar().setTitle("Carrito");
        lblSubTotal = view.findViewById(R.id.lbl_valor_sub_total);
        lblTotalIva = view.findViewById(R.id.lbl_valor_total_iva);
        detallePedido = CarritoDataController.getInstance().productos;
        btnCancelar = view.findViewById(R.id.btn_cancelar);
        btnPedido = view.findViewById(R.id.btn_aceptar);
        btnCancelar.setOnClickListener(this);
        btnPedido.setOnClickListener(this);

        lblTotal = view.findViewById(R.id.lbl_valor_total);
        recyclerView = view.findViewById(R.id.recycler_pedido);
        recyclerView.setLayoutManager(new LinearLayoutManager(CarritoFragment.this.getActivity()));
        if (detallePedido != null) {
            adapter = new CarritoAdapter(detallePedido, this);
            recyclerView.setAdapter(adapter);
            /*  total.setText("" + calcularTotal(detallePedido));*/
            calcularTotaltes();
        } else {
            lblTotal.setText("$ " + 0);
        }
        btnCancelar = view.findViewById(R.id.btn_aceptar);
        btnPedido = view.findViewById(R.id.btn_aceptar);
    }


    public void updateAdapter(ArrayList<Producto> array) {
        adapter.setListaProductos(array);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onResume() {
        super.onResume();
        ((CatalogoActivity) this.getActivity()).getSupportActionBar().setTitle("Carrito");
        detallePedido = CarritoDataController.getInstance().productos;
        if (detallePedido != null) {
            adapter = new CarritoAdapter(detallePedido, this);
            recyclerView.setAdapter(adapter);
            calcularTotaltes();
        }
        calcularTotaltes();
    }

    @Override
    public void numberProducto(int position, int cantidad2) {

        int cantidad = detallePedido.get(position).getCantidad() + cantidad2;

        if (cantidad2 == 0) {
            CarritoDataController.getInstance().removeProducto(detallePedido.get(position));
            updateAdapter(CarritoDataController.getInstance().productos);
            calcularTotaltes();
            ((CatalogoActivity) getActivity()).mostrarCantidad(0);
            CarritoFragment.this.getActivity().getWindow().getDecorView().clearFocus();
        } else {

            if ((cantidad - 1) > 0) {

                detallePedido.get(position).setCantidad(cantidad2);
                CarritoDataController.getInstance().updateProducto(detallePedido.get(position));
                updateAdapter(detallePedido);
                calcularTotaltes();
                int n = CarritoDataController.getInstance().getCantidadCarrito();
                ((CatalogoActivity) getActivity()).mostrarCantidad(n);
                CarritoFragment.this.getActivity().getWindow().getDecorView().clearFocus();
            } else if ((cantidad - 1) == 0) {

                CarritoDataController.getInstance().removeProducto(detallePedido.get(position));
                updateAdapter(CarritoDataController.getInstance().productos);
                calcularTotaltes();
                ((CatalogoActivity) getActivity()).mostrarCantidad(0);

                CarritoFragment.this.getActivity().getWindow().getDecorView().clearFocus();
            }
        }

    }


    @Override
    public void onMinusClicked(int position) {

        int cantidad = detallePedido.get(position).getCantidad();

        if ((cantidad - 1) > 0) {
            detallePedido.get(position).setCantidad(cantidad - 1);
            CarritoDataController.getInstance().updateProducto(detallePedido.get(position));
            updateAdapter(detallePedido);
            /*  total.setText("" + calcularTotal(detallePedido));*/
            calcularTotaltes();
            int n = CarritoDataController.getInstance().getCantidadCarrito();
            ((CatalogoActivity) getActivity()).mostrarCantidad(n);

        } else if ((cantidad - 1) == 0) {

            CarritoDataController.getInstance().removeProducto(detallePedido.get(position));
            updateAdapter(CarritoDataController.getInstance().productos);
            /*total.setText("" + calcularTotal(detallePedido));*/
            calcularTotaltes();
            ((CatalogoActivity) getActivity()).mostrarCantidad(0);
        }

    }

    @Override
    public void onPlusClicked(int position) {
        int cantidad = detallePedido.get(position).getCantidad() + 1;
        detallePedido.get(position).setCantidad(cantidad);
        CarritoDataController.getInstance().updateProducto(detallePedido.get(position));
        updateAdapter(detallePedido);
        /* total.setText("" + calcularTotal(detallePedido));*/
        calcularTotaltes();
        int n = CarritoDataController.getInstance().getCantidadCarrito();
        ((CatalogoActivity) getActivity()).mostrarCantidad(n);
    }


    public float calcularTotalFloat(ArrayList<Producto> detallePedido) {
        float total = 0;
        for (int i = 0; i < detallePedido.size(); i++) {
            total += Double.parseDouble(detallePedido.get(i).getPrecio()) * detallePedido.get(i).getCantidad();
        }
        return total;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_aceptar_observacion:
                observaciones = txtObservaciones.getText().toString();
                enviarPedido();
                break;

            case R.id.btn_cancelar:
                //   DataController.getInstance().setListaPedido(new ArrayList<>());
                Util.showDialog(CarritoFragment.this.getActivity(), "Alerta", "¿Está seguro que desea eliminar el pedido?", "Aceptar", "Cancelar", true, true,
                        new Callable<Void>() {
                            @Override
                            public Void call() throws Exception {
                                CarritoDataController.getInstance().productos.clear();
                                detallePedido.clear();
                                updateAdapter(detallePedido);
                                lblTotal.setText("$ 0");
                                lblTotalIva.setText("$ 0");
                                lblSubTotal.setText("$ 0");
                                ((CatalogoActivity) getActivity()).mostrarCantidad(0);
                                return null;
                            }
                        }, null);
                break;

            case R.id.btn_aceptar:
                if (detallePedido != null) {
                    if (detallePedido.size() > 0) {
                        double pedidoMin = DataController.getInstance().getProveedor().getPedido_minimo();
                        if (totalPedido > pedidoMin) {
                            int pedidoMax = DataController.getInstance().getProveedor().getMax_pedidos();
                            int pedidosDia = DataController.getInstance().getPedidosDia().size();
                            if (DataController.getInstance().getPedidosDia().size() < pedidoMax) {
                                Util.showDialog(CarritoFragment.this.getActivity(), "Alerta", "Sólo puede realizar una cantidad de " + pedidoMax + " al día. ¿Desea continuar?", "Aceptar", "Cancelar", true, true,
                                        new Callable<Void>() {
                                            @Override
                                            public Void call() throws Exception {
                                                showCustomDialog();
                                                return null;
                                            }
                                        }, null);
                            } else {
                                Util.showDialog(CarritoFragment.this.getActivity(), "Alerta", "Ya realizó su número máximo de pedidos(" + pedidoMax + ") por día.", "Aceptar", "", true, false, null, null);
                            }

                        } else {
                            Util.showDialog(CarritoFragment.this.getActivity(), "Alerta", "El valor total del pedido debe ser mayor a " + Util.getFormatCurrency(pedidoMin), "Aceptar", "", true, false, null, null);
                        }
                    } else {
                        Util.showDialog(CarritoFragment.this.getActivity(), "Alerta", "No hay productos en el carrito", "Aceptar", "", true, false, null, null);
                    }
                } else {
                    Util.showDialog(CarritoFragment.this.getActivity(), "Alerta", "No hay productos en el carrito", "Aceptar", "", true, false, null, null);
                }


                break;
        }
    }


    public void enviarPedido() {
        ProgressView2.show(CarritoFragment.this.getActivity());
        if (!Util.checkInternet(CarritoFragment.this.getActivity())) {
            Util.showDialog(CarritoFragment.this.getActivity(), "Alerta", "No Hay conexión a internet", "Aceptar", "", true, false, null, null);
            ProgressView2.dismiss();
            return;
        }

        try {

            String params = "?codigo=" + DataController.getInstance().getProveedor().getCodigo_empresa();
            String url = Util.getUrl(params, DataController.getInstance().getProveedor(), APIs.PEDIDO_END);
            String body = crearCuerpoJson();

            Networking.post(url, body, new Networking.ICallback() {
                @Override
                public void onFail(String error) {
                    ProgressView2.dismiss();
                    Util.showDialog(CarritoFragment.this.getActivity(), "Alerta", "No se pudo descargar la lista de productos", "Aceptar", "", true, false, null, null);
                }

                @Override
                public void onSuccess(String response) {
                    ProgressView2.dismiss();
                    Util.showDialog(CarritoFragment.this.getActivity(), "Información", "Su pedido fue registrado con éxito. Recuerda que tu pedido sera enviado segun tu frecuencia de visita!", "Aceptar", "", true, false, new Callable<Void>() {
                        @Override
                        public Void call() throws Exception {
                            DataController.getInstance().setListaPedido(new ArrayList<>());
                            detallePedido.clear();
                            updateAdapter(detallePedido);
                            lblTotal.setText("$ 0");
                            Intent intent = new Intent(CarritoFragment.this.getActivity(), MainActivity.class);
                            startActivity(intent);
                            CarritoFragment.this.getActivity().finish();
                            return null;
                        }
                    }, null);
                }
            });

        } catch (JSONException e) {
            ProgressView2.dismiss();
            Util.showDialog(CarritoFragment.this.getActivity(), "Alerta", "No se pudo descargar la lista de productos", "Aceptar", "", true, false, null, null);
            e.printStackTrace();
        }


    }

    public String crearCuerpoJson() throws JSONException {

        Pedido pedido = CarritoDataController.getInstance().generarPedido();
        Gson gson = new Gson();
        String listString = gson.toJson(pedido.getDetalle(), new TypeToken<ArrayList<ListaDetalle>>() {
        }.getType());
        JSONArray jsonArray = new JSONArray(listString);

        String body = "{ \"ListaDetalle\": [";

        for (int i = 0; i < jsonArray.length(); i++) {

            JSONObject object = new JSONObject();
            JSONObject info = jsonArray.getJSONObject(i);
            object.put("NumeroDoc", info.getLong("NumeroDoc"));
            object.put("CodigoCliente", info.getString("CodigoCliente"));
            object.put("CodigoProveedor", info.getInt("CodigoProveedor"));
            object.put("FechaMovil", info.getString("FechaMovil"));
            object.put("Posicion", info.getInt("Posicion"));
            object.put("Cantidad", info.getInt("Cantidad"));
            object.put("CodigoProducto", info.getString("codigo_producto"));
            object.put("Precio", info.getLong("Precio"));
            object.put("Iva", info.getInt("iva"));
            object.put("Observacion", observaciones);

            body += object.toString();
            if (i < jsonArray.length() - 1) {
                body += ",";
            }
        }
        body += "]}";

        Log.d("POST_BODY", body);
        return body;
    }

    public void calcularTotaltes() {
        ArrayList<Producto> listaPedido = CarritoDataController.getInstance().productos;
        double subtotal = 0;
        double totalIva = 0;


        for (int i = 0; i < listaPedido.size(); i++) {
            subtotal += listaPedido.get(i).getCantidad() * Double.parseDouble(listaPedido.get(i).getPrecio());
            totalIva += Double.parseDouble(Util.round(listaPedido.get(i).getCantidad() * Double.parseDouble(listaPedido.get(i).getPrecio()) * ((double) listaPedido.get(i).getIva() / 100), 0));
        }

        lblTotalIva.setText(Util.getFormatCurrency(totalIva));
        lblSubTotal.setText(Util.getFormatCurrency(subtotal));
        lblTotal.setText(Util.getFormatCurrency(subtotal + totalIva));
        totalPedido = subtotal + totalIva;
    }


    private void showCustomDialog() {
        ViewGroup viewGroup = view.findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(CarritoFragment.this.getActivity()).inflate(R.layout.dialogo_observaciones, viewGroup, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(CarritoFragment.this.getActivity());
        builder.setView(dialogView);

        Button aceptarDialogo = dialogView.findViewById(R.id.btn_aceptar_observacion);
        txtObservaciones = dialogView.findViewById(R.id.txt_observaciones_dialogo);
        aceptarDialogo.setOnClickListener(this);


        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
}
