package com.celuweb.emart.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.celuweb.emart.Adapter.CatalogoAdapter;
import com.celuweb.emart.DataController.DataController;
import com.celuweb.emart.DataObject.Categoria;
import com.celuweb.emart.DataObject.Producto;
import com.celuweb.emart.Emart.CatalogoActivity;
import com.celuweb.emart.R;
import com.celuweb.emart.Util.ProgressView;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;

public class CatalogoFragment extends Fragment implements TabLayout.OnTabSelectedListener {


    private TabLayout tabLayout;
    private SearchView searchView;


    private ArrayList<Categoria> categorias;
    private ArrayList<Producto> productos = new ArrayList<>();
    private ArrayList<Producto> productosBusqueda = new ArrayList<>();
    private RecyclerView catalogoRecyclerView;
    private CatalogoAdapter adapter;
    private ArrayList<Producto> listaProductos;
    private int tabIndex = 0;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_catalogo, container, false);

        view.findViewById(R.id.codigoBarras).setOnClickListener(mOnClickBarCode);
        initActionBar();
        initUI(view);

        return view;
    }

    public CatalogoFragment() {
    }

    private void initActionBar() {
        ((CatalogoActivity) getActivity()).getSupportActionBar().setTitle("Catalogo");
    }

    private void initUI(View view) {
        ((CatalogoActivity) this.getActivity()).getToolbar().setTitle("Catálogo");
        catalogoRecyclerView = view.findViewById(R.id.recyclerView);
        catalogoRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 3));

        tabLayout = view.findViewById(R.id.tabLayout);
        tabLayout.setOnTabSelectedListener(this);

        initTabLayout();

        searchView = view.findViewById(R.id.searchView);
        searchView.setQueryHint("Buscar Producto...");
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                String userInput = s.toLowerCase();
                productosBusqueda = new ArrayList<>();

                for (Producto producto : productos) {
                    if (producto.getNombre().toLowerCase().contains(userInput)) {
                        productosBusqueda.add(producto);
                    }
                }

                adapter.productos = productosBusqueda;
                adapter.notifyDataSetChanged();

                TabLayout.Tab tab = tabLayout.getTabAt(categorias.size());
                tab.select();

                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });
        searchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    private void initTabLayout() {
        categorias = DataController.getInstance().getListaCategoria();

        int tabCount = tabLayout.getTabCount();

        if (tabCount <= 0) {
            int i = 0;
            do {
                tabLayout.addTab(tabLayout.newTab().setText(categorias.get(i).getDescripcion()));
                i++;
            } while (i < categorias.size());

            //tabLayout.addTab(tabLayout.newTab().setText("Busqueda"));

        }
        adapter.notifyDataSetChanged();
        ProgressView.getInstance().Dismiss();

    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {

        if (tab.getPosition() != (categorias.size())) {

            productos = listaProductos;
            adapter.productos = productos;

            catalogoRecyclerView.setAdapter(adapter);
            adapter.notifyDataSetChanged();

            tabIndex = tab.getPosition();
        } else {
            adapter.productos = productosBusqueda;

            catalogoRecyclerView.setAdapter(adapter);
            adapter.notifyDataSetChanged();

            tabIndex = tab.getPosition();
        }
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    public void ActualizarCantidad() {
//        final int c = dataBaseHelperClass.obtenerCantidadDetalleTemp();
//        ((MainActivity) getActivity()).actualizarCantidadCarrito(c);

    }


    private View.OnClickListener mOnClickBarCode = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

//            IntentIntegrator intentIntegrator = new IntentIntegrator(CatalogoFragment.this.getActivity());
//            intentIntegrator.setDesiredBarcodeFormats(IntentIntegrator.PRODUCT_CODE_TYPES);
//            intentIntegrator.setPrompt("Scan");
//            intentIntegrator.setCameraId(0);
//            intentIntegrator.setBeepEnabled(true);
//            intentIntegrator.setBarcodeImageEnabled(false);
//            intentIntegrator.initiateScan();

        }
    };

    public boolean BuscarBarras(String s) {
        String code = s.toLowerCase();
        productosBusqueda = new ArrayList<>();

        for (Producto producto : productos) {
            if (producto.getCodigo().contains(code)) {
                productosBusqueda.add(producto);
            }
        }

        adapter.productos = productosBusqueda;
        adapter.notifyDataSetChanged();

        TabLayout.Tab tab = tabLayout.getTabAt(categorias.size());
        tab.select();

        return true;
    }


}
