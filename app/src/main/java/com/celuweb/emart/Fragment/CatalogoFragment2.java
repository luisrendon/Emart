package com.celuweb.emart.Fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.celuweb.emart.Adapter.CatalogoAdapter;
import com.celuweb.emart.Adapter.ListaAdapter;
import com.celuweb.emart.DataController.CarritoDataController;
import com.celuweb.emart.DataController.DataController;
import com.celuweb.emart.DataObject.Categoria;
import com.celuweb.emart.DataObject.ConsultaProductos;
import com.celuweb.emart.DataObject.Producto;
import com.celuweb.emart.Emart.CatalogoActivity;
import com.celuweb.emart.Networking.APIs;
import com.celuweb.emart.Networking.Networking;
import com.celuweb.emart.R;
import com.celuweb.emart.Util.ProgressView;
import com.celuweb.emart.Util.Util;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;

import java.util.ArrayList;

public class CatalogoFragment2 extends Fragment implements TabLayout.OnTabSelectedListener {

    private LinearLayout linearLayout, enviarLinearLayout;

    private ArrayList<Producto> productos = new ArrayList<>();
    private ViewGroup transitionsContainer;
    private RecyclerView lista;
    private ArrayList<Categoria> categorias;

    private TabLayout tabLayout;
    private int tabIndex = 0;

    private CatalogoAdapter adapter;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_lista, container, false);
        initActionBar();
        init(view);
        initUI();
        return view;
    }

    private void initActionBar() {
        ((CatalogoActivity) getActivity()).getSupportActionBar().setTitle("Lista Catálogo");
    }

    private void init(final View view) {
        transitionsContainer = view.findViewById(R.id.constraintLayout);
        linearLayout = view.findViewById(R.id.linearLayout);

        lista = view.findViewById(R.id.listaRecyclerView);
        lista.setLayoutManager(new GridLayoutManager(getContext(), 2));

        tabLayout = view.findViewById(R.id.tabLayout);
        tabLayout.setOnTabSelectedListener(this);

        adapter = new CatalogoAdapter(productos, this);
        lista.setAdapter(adapter);

        initTabLayout();
    }

    private void initUI() {

    }

    private void initTabLayout() {
        categorias = DataController.getInstance().getListaCategoria();

        int tabCount = tabLayout.getTabCount();

        if (tabCount <= 0) {
            int i = 0;
            do {
                tabLayout.addTab(tabLayout.newTab().setText(categorias.get(i).getDescripcion()));
                i++;
            } while (i < categorias.size());

           // tabLayout.addTab(tabLayout.newTab().setText("Busqueda"));

        }
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        if (tab.getPosition() != (categorias.size())) {
            tabIndex = tab.getPosition();
            Log.d("index", tabIndex + "");
            consultarProductos(categorias.get(tabIndex));
        } else {

        }
    }

    private void consultarProductos(Categoria categoria) {
        String origen = DataController.getInstance().getProveedor().getCodigo_empresa();
        String cliente = DataController.getInstance().getProveedor().getCodigo();
        int page = 1;
        String ca = categoria.getCodigo();

        String params = "?origen=" + origen + "&cliente=" + cliente + "&page=" + page + "&ca=" + ca;
        String url = Util.getUrl(params, DataController.getInstance().getProveedor(), APIs.BUSCAR_END);

        ProgressView.getInstance().Show(getActivity(), new ProgressView.ICallback() {
            @Override
            public void run() {
                Networking.get(url, new Networking.ICallback() {
                    @Override
                    public void onFail(String error) {
                        ProgressView.getInstance().Dismiss();
                    }

                    @Override
                    public void onSuccess(String response) {

                        ProgressView.getInstance().Dismiss();

                        ConsultaProductos consultaProductos = new Gson().fromJson(response, ConsultaProductos.class);
                        actualizarProductos(consultaProductos.getResult());
                    }
                });
            }
        });
    }

    private void actualizarProductos(ArrayList<Producto> productos) {
        this.productos = productos;

        adapter.productos = productos;
        adapter.notifyDataSetChanged();
    }

    public void actualizarProductoLista(Producto producto) {
        int n = CarritoDataController.getInstance().getCantidadCarrito();
        ((CatalogoActivity) getActivity()).mostrarCantidad(n);

        for (Producto p : productos) {
            if (p.getCodigo().equals(producto.getCodigo())) {
                p.setCantidad(producto.getCantidad());
                return;
            }
        }

        adapter.notifyDataSetChanged();
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

}
