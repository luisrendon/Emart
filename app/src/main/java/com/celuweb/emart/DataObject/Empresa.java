package com.celuweb.emart.DataObject;

import java.io.Serializable;
import java.util.ArrayList;

public class Empresa implements Serializable {
    private int num_empresa;
    private String codigo_empresa;
    private String nombre_empresa;
    private String tipo;
    private String url;
    private String path;
    private String imagen;
    private float pedido_minimo;
    private int max_pedidos;
    ArrayList<Sucursal> sucursales = new ArrayList<>();


    // Getter Methods

    public int getNum_empresa() {
        return num_empresa;
    }

    public String getCodigo_empresa() {
        return codigo_empresa;
    }

    public String getNombre_empresa() {
        return nombre_empresa;
    }

    public String getTipo() {
        return tipo;
    }

    public String getUrl() {
        return url;
    }

    public String getPath() {
        return path;
    }

    public String getImagen() {
        return imagen;
    }

    public float getPedido_minimo() {
        return pedido_minimo;
    }

    public int getMax_pedidos() {
        return max_pedidos;
    }

    // Setter Methods

    public void setNum_empresa(int num_empresa) {
        this.num_empresa = num_empresa;
    }

    public void setCodigo_empresa(String codigo_empresa) {
        this.codigo_empresa = codigo_empresa;
    }

    public void setNombre_empresa(String nombre_empresa) {
        this.nombre_empresa = nombre_empresa;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public void setPedido_minimo(float pedido_minimo) {
        this.pedido_minimo = pedido_minimo;
    }

    public void setMax_pedidos(int max_pedidos) {
        this.max_pedidos = max_pedidos;
    }


    public ArrayList<Sucursal> getSucursales() {
        return sucursales;
    }

    public void setSucursales(ArrayList<Sucursal> sucursales) {
        this.sucursales = sucursales;
    }
}
