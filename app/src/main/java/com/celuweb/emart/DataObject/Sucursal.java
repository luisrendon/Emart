package com.celuweb.emart.DataObject;

import java.io.Serializable;

public class Sucursal implements Serializable {

    private String nombre;
    private String codigo;
    private String razon_social;
    private String telefono;
    private String direccion;
    private String ciudad;
    private String vendedor;


    // Getter Methods

    public String getNombre() {
        return nombre;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getRazon_social() {
        return razon_social;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public String getCiudad() {
        return ciudad;
    }

    public String getVendedor() {
        return vendedor;
    }

    // Setter Methods

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public void setRazon_social(String razon_social) {
        this.razon_social = razon_social;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public void setVendedor(String vendedor) {
        this.vendedor = vendedor;
    }
}
