package com.celuweb.emart.DataObject;

public class PedidoDetalle {

    private long NumeroDoc;
    private String CodigoCliente;
    private int CodigoProveedor;
    private String FechaMovil;
    private int Posicion;
    private int Cantidad;
    private String CodigoProducto;
    private String nombreProducto;
    private Double precio;


    public String getNombreProducto() {
        return nombreProducto;
    }

    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public long getNumeroDoc() {
        return NumeroDoc;
    }

    public void setNumeroDoc(long numeroDoc) {
        NumeroDoc = numeroDoc;
    }

    public String getCodigoCliente() {
        return CodigoCliente;
    }

    public void setCodigoCliente(String codigoCliente) {
        CodigoCliente = codigoCliente;
    }

    public int getCodigoProveedor() {
        return CodigoProveedor;
    }

    public void setCodigoProveedor(int codigoProveedor) {
        CodigoProveedor = codigoProveedor;
    }

    public String getFechaMovil() {
        return FechaMovil;
    }

    public void setFechaMovil(String fechaMovil) {
        FechaMovil = fechaMovil;
    }

    public int getPosicion() {
        return Posicion;
    }

    public void setPosicion(int posicion) {
        Posicion = posicion;
    }

    public int getCantidad() {
        return Cantidad;
    }

    public void setCantidad(int cantidad) {
        Cantidad = cantidad;
    }

    public String getCodigoProducto() {
        return CodigoProducto;
    }

    public void setCodigoProducto(String codigoProducto) {
        CodigoProducto = codigoProducto;
    }
}
