package com.celuweb.emart.DataObject;

import java.util.ArrayList;

public class Pedido {

    private long numero_doc;
    private String codigo_cliente;
    private int codigo_proveedor;
    private String fecha_movil;
    ArrayList<ListaDetalle> detalle;

    public Pedido(ArrayList<com.celuweb.emart.DataObject.ListaDetalle> listaDetalle) {
        detalle = listaDetalle;
    }

    public ArrayList<com.celuweb.emart.DataObject.ListaDetalle> getDetalle() {
        return detalle;
    }

    public void setDetalle(ArrayList<com.celuweb.emart.DataObject.ListaDetalle> detalle) {
        this.detalle = detalle;
    }

    public long getNumero_doc() {
        return numero_doc;
    }

    public void setNumero_doc(long numero_doc) {
        this.numero_doc = numero_doc;
    }

    public String getCodigo_cliente() {
        return codigo_cliente;
    }

    public void setCodigo_cliente(String codigo_cliente) {
        this.codigo_cliente = codigo_cliente;
    }

    public int getCodigo_proveedor() {
        return codigo_proveedor;
    }

    public void setCodigo_proveedor(int codigo_proveedor) {
        this.codigo_proveedor = codigo_proveedor;
    }

    public String getFecha_movil() {
        return fecha_movil;
    }



    public void setFecha_movil(String fecha_movil) {
        this.fecha_movil = fecha_movil;
    }
}
