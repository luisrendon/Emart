package com.celuweb.emart.DataObject;

public class Proveedor {
    private String nombre;
    private String codigo;
    private String razonsocial;
    private int num_empresa;
    private String nombre_empresa;
    private String tipo;
    private String telefono;
    private String url;
    private String path;
    private String imagen;
    private String codigo_empresa;
    private float pedido_minimo;
    private int max_pedidos;

    public float getPedido_minimo() {
        return pedido_minimo;
    }

    public void setPedido_minimo(float pedido_minimo) {
        this.pedido_minimo = pedido_minimo;
    }

    public int getMax_pedidos() {
        return max_pedidos;
    }

    public void setMax_pedidos(int max_pedidos) {
        this.max_pedidos = max_pedidos;
    }

    public String getCodigo_empresa() {
        return codigo_empresa;
    }

    public void setCodigo_empresa(String codigo_empresa) {
        this.codigo_empresa = codigo_empresa;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getRazonsocial() {
        return razonsocial;
    }

    public void setRazonsocial(String razonsocial) {
        this.razonsocial = razonsocial;
    }

    public int getNum_empresa() {
        return num_empresa;
    }

    public void setNum_empresa(int num_empresa) {
        this.num_empresa = num_empresa;
    }

    public String getNombre_empresa() {
        return nombre_empresa;
    }

    public void setNombre_empresa(String nombre_empresa) {
        this.nombre_empresa = nombre_empresa;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }
}
