package com.celuweb.emart.DataObject;

public class ListaDetalle {

    private String NumeroDoc;
    private String CodigoCliente;
    private String CodigoProveedor;
    private String FechaMovil;
    private int Posicion;
    private long Precio;
    private int Cantidad;
    public long precio;//se crea porque el servicio de productodia trae los campos con el nombre en minuscula
    public int cantidad;//se crea porque el servicio de productodia trae los campos con el nombre en minuscula
    public int posicion;//se crea porque el servicio de productodia trae los campos con el nombre en minuscula
    private String codigo_producto;
    private int iva;
    private String nombre_producto;

    public ListaDetalle(String numeroDoc, String codigoCliente, String codigoProveedor, String fechaMovil, int posicion, int cantidad, String codigo_producto, long Precio, int iva) {
        NumeroDoc = numeroDoc;
        CodigoCliente = codigoCliente;
        CodigoProveedor = codigoProveedor;
        FechaMovil = fechaMovil;
        Posicion = posicion;
        Cantidad = cantidad;
        this.codigo_producto = codigo_producto;
        this.Precio = Precio;
        this.iva = iva;
    }


    public String getNumeroDoc() {
        return NumeroDoc;
    }

    public void setNumeroDoc(String numeroDoc) {
        NumeroDoc = numeroDoc;
    }

    public String getCodigoCliente() {
        return CodigoCliente;
    }

    public void setCodigoCliente(String codigoCliente) {
        CodigoCliente = codigoCliente;
    }

    public String getCodigoProveedor() {
        return CodigoProveedor;
    }

    public void setCodigoProveedor(String codigoProveedor) {
        CodigoProveedor = codigoProveedor;
    }

    public String getFechaMovil() {
        return FechaMovil;
    }

    public void setFechaMovil(String fechaMovil) {
        FechaMovil = fechaMovil;
    }

    public int getPosicion() {
        return Posicion;
    }

    public void setPosicion(int posicion) {
        Posicion = posicion;
    }

    public int getCantidad() {
        return Cantidad;
    }

    public void setCantidad(int cantidad) {
        Cantidad = cantidad;
    }

    public String getCodigo_producto() {
        return codigo_producto;
    }

    public void setCodigo_producto(String codigo_producto) {
        this.codigo_producto = codigo_producto;
    }

    public String getNombre_producto() {
        return nombre_producto;
    }

    public long getPrecio() {
        return Precio;
    }

    public void setPrecio(long precio) {
        Precio = precio;
    }

    public int getIva() {
        return iva;
    }

    public void setIva(int iva) {
        this.iva = iva;
    }

    public void setNombre_producto(String nombre_producto) {
        this.nombre_producto = nombre_producto;
    }
}
