package com.celuweb.emart.DataObject;

import java.util.ArrayList;

public class UltimoPedido {

    private String numero_doc;
    private String codigo;
    private String fecha;
    private ArrayList<DetalleUltimoPedido> detalle;

    public String getNumeroDoc(){
        return numero_doc;
    }

    public String getFecha(){
        return fecha;
    }


    public String getCodigo(){
        return codigo;
    }


    public ArrayList<DetalleUltimoPedido> getDetalles(){
        return detalle;
    }

}
