package com.celuweb.emart.DataObject;

import java.util.ArrayList;

public class ConsultaProductos {
    private long total;
    private long pages;
    private long next;
    private long prev;
    private long now;
    private ArrayList<Producto> result;

    public long getTotal() { return total; }
    public void setTotal(long value) { this.total = value; }

    public long getPages() { return pages; }
    public void setPages(long value) { this.pages = value; }

    public long getNext() { return next; }
    public void setNext(long value) { this.next = value; }

    public long getPrev() { return prev; }
    public void setPrev(long value) { this.prev = value; }

    public long getNow() { return now; }
    public void setNow(long value) { this.now = value; }

    public ArrayList<Producto> getResult() { return result; }
    public void setResult(ArrayList<Producto> value) { this.result = value; }
}
