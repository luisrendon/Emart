package com.celuweb.emart.DataObject;

import java.util.List;

public class Activar {
    private int codigo;
    private int activo;
    private List<String> telefonos;

    public long getCodigo() { return codigo; }
    public void setCodigo(int value) { this.codigo = value; }

    public long getActivo() { return activo; }
    public void setActivo(int value) { this.activo = value; }

    public List<String> getTelefonos() { return telefonos; }
    public void setTelefonos(List<String> value) { this.telefonos = value; }
}
