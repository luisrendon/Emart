package com.celuweb.emart.Adapter;

import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.celuweb.emart.DataObject.Activar;
import com.celuweb.emart.DataObject.Cartera;
import com.celuweb.emart.Emart.LoginActivity;
import com.celuweb.emart.Emart.MainActivity;
import com.celuweb.emart.R;
import com.celuweb.emart.Util.Util;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


public class ActivarAdapter extends RecyclerView.Adapter<ActivarAdapter.ViewHolder> {
    public ArrayList<String> telefono;
    private ActivarEvent listener;

    public ActivarAdapter(ArrayList<String> telefonos, ActivarEvent listener) {
        this.telefono = telefonos;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_activar, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.numeroTextView.setText(telefono.get(i).toString());

        viewHolder.sendNumero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Util.showDialog(view.getContext(), "Alerta", "Desea enviar el Mensaje a este numero: \n "+ telefono.get(i).toString(), "Aceptar", "Cancelar", true, true, new Callable<Void>() {
                    @Override
                    public Void call() throws Exception {

                        String numero = telefono.get(i).toString();
                        listener.enviarSMS(numero);
                        return null;
                    }
                }, null);
//                Toast.makeText(view.getContext(), telefono.get(i).toString()+" Selecionado", Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public int getItemCount() {
        return telefono.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView numeroTextView;
        private ImageView sendNumero;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            numeroTextView = itemView.findViewById(R.id.numeroTextView);
            sendNumero = itemView.findViewById(R.id.btn_enviar_numero);

        }

    }
    public interface ActivarEvent {

        void enviarSMS(String numero);
    }



}


