package com.celuweb.emart.Adapter;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.content.Context;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;

import com.celuweb.emart.DataObject.Empresa;
import com.celuweb.emart.DataObject.Proveedor;
import com.celuweb.emart.R;

import java.util.ArrayList;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;


public class ListaProveedoresAdapter  extends RecyclerView.Adapter<ListaProveedoresAdapter.ViewHolder> {

    private ArrayList<Empresa> listaProveedores;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    Context contexto;

    // data is passed into the constructor
    public ListaProveedoresAdapter(Context context, ArrayList<Empresa> listaProveedores) {
        this.mInflater = LayoutInflater.from(context);
        this.listaProveedores = listaProveedores;
        this.contexto = context;
    }

    // inflates the cell layout from xml when needed
    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.recyclerview_item, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each cell
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
       // holder.myTextView.setText(listaProveedores.get(position).getNombre_empresa());
        holder.myTextView.setText("");


        Glide.with(contexto).load("http://66.33.94.211/logo/"+listaProveedores.get(position).getCodigo_empresa()+"/"+listaProveedores.get(position).getCodigo_empresa()+".png")
                //.placeholder(R.drawable.ic_holder)
                .dontAnimate()
                //.thumbnail(1.0f)
                .override(90, 90)
                //.sizeMultiplier(1.0f)
                //.crossFade()
                .fitCenter()
                .placeholder(R.drawable.placeholder)
                .error(android.R.drawable.gallery_thumb)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.imageView);

    }

    // total number of cells
    @Override
    public int getItemCount() {
        return listaProveedores.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView myTextView;
        ImageView imageView;

        ViewHolder(View itemView) {
            super(itemView);
            myTextView = itemView.findViewById(R.id.info_text);
            imageView = itemView.findViewById(R.id.imageView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    Empresa getItem(int id) {
        return listaProveedores.get(id);
    }

    // allows clicks events to be caught
   public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
