package com.celuweb.emart.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.celuweb.emart.DataObject.Proveedor;
import com.celuweb.emart.DataObject.UltimoPedido;
import com.celuweb.emart.R;

import java.util.ArrayList;


public class UltimoPedidoAdapter extends RecyclerView.Adapter<UltimoPedidoAdapter.ViewHolder> {

    private ArrayList<UltimoPedido> listaDeUltimosPedidos;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    Context contexto;

    // data is passed into the constructor
    public UltimoPedidoAdapter(Context context,ArrayList<UltimoPedido> listaDeUltimosPedidos) {
        this.mInflater = LayoutInflater.from(context);
        this.listaDeUltimosPedidos = listaDeUltimosPedidos;
        this.contexto = context;
    }

    // inflates the cell layout from xml when needed
    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.recyclerview_item1, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each cell
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.myTextView.setText("Fecha Pedido: "+listaDeUltimosPedidos.get(position).getFecha());
        holder.myTextView1.setText("Num Documento: "+listaDeUltimosPedidos.get(position).getNumeroDoc());
    }

    // total number of cells
    @Override
    public int getItemCount() {
        return listaDeUltimosPedidos.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView myTextView;
        TextView myTextView1;

        ViewHolder(View itemView) {
            super(itemView);
            myTextView = itemView.findViewById(R.id.info_text);
            myTextView1 = itemView.findViewById(R.id.info_text1);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    UltimoPedido getItem(int id) {
        return listaDeUltimosPedidos.get(id);
    }

    // allows clicks events to be caught
   public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
