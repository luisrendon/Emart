package com.celuweb.emart.Adapter;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.celuweb.emart.DataController.CarritoDataController;
import com.celuweb.emart.DataObject.Producto;
import com.celuweb.emart.Fragment.CatalogoFragment;
import com.celuweb.emart.Fragment.CatalogoFragment2;
import com.celuweb.emart.Fragment.ListaFragment;
import com.celuweb.emart.R;
import com.celuweb.emart.Util.Util;

import java.text.NumberFormat;
import java.util.ArrayList;

public class CatalogoAdapter extends RecyclerView.Adapter<CatalogoAdapter.ViewHolder> {

    public ArrayList<Producto> productos;
    public CatalogoFragment2 listaFragment;

    public CatalogoAdapter(ArrayList<Producto> productos, CatalogoFragment2 listaFragment) {
        this.productos = productos;
        this.listaFragment = listaFragment;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.catalogo_item, parent, false);
        return new CatalogoAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        productos.get(position).setCantidad(CarritoDataController.getInstance().getCantidadProducto(productos.get(position)));
        holder.eanTextView.setText("Código de Barras: " + productos.get(position).getEan());
        holder.codigoTextView.setText("Código Interno: " + productos.get(position).getCodigo());
        holder.nombreProductoTextView.setText(productos.get(position).getNombre());
        holder.precioProductoTextView.setText(Util.getFormatCurrency(Double.parseDouble(productos.get(position).getPrecio())));


        descargarImagen(holder, position);

        if (productos.get(position).getCantidad() > 0) {
            activarPanelCantidad(holder);
            holder.cantidadTextView.setText(productos.get(position).getCantidad() + "");
        } else {
            desactivarPanelCantidad(holder);
        }
        holder.cantidadTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                numberProducto(holder, position);
            }
        });

        holder.agregarButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addProducto(holder, position);
            }
        });

        holder.addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                plusProducto(holder, position);
            }
        });

        holder.removeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                minusProducto(holder, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return productos.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private void descargarImagen(CatalogoAdapter.ViewHolder holder, int position) {
        //String url = "http://66.33.94.209/ServiceTiendaYa/Imgs/" + productos.get(position).getCodigo() +"_"+ ".png";
        String url = "http://66.33.94.204/SyncEmart/Imgs/7002/" + productos.get(position).getCodigo() + ".jpg";
        Glide.with(holder.itemView.getContext())
                .load(url)
                .centerCrop()
                .placeholder(R.drawable.placeholder)
                .apply(new RequestOptions().override(200, 200))
                .into(holder.productoImageView);
    }

    private void resetCell(CatalogoAdapter.ViewHolder holder) {
        desactivarPanelCantidad(holder);
    }

    private void numberProducto(CatalogoAdapter.ViewHolder holder, int position) {
        int cantidad = CarritoDataController.getInstance().getCantidadProducto(productos.get(position));
        int editText = 0;
        try {
            editText = Integer.parseInt(holder.cantidadTextView.getText().toString());
        } catch (NumberFormatException e) {
            editText = 0;
        }


        if (cantidad > 0) {
            productos.get(position).setCantidad(cantidad);
            productos.get(position).setCantidad(editText);

            if (productos.get(position).getCantidad() > 0) {
                holder.cantidadTextView.setText(productos.get(position).getCantidad() + "");
                CarritoDataController.getInstance().addProducto(productos.get(position));
                listaFragment.actualizarProductoLista(productos.get(position));
                EditText et = holder.cantidadTextView;
                et.setSelection(et.getText().length());
            } else {
                productos.get(position).setCantidad(0);
                listaFragment.actualizarProductoLista(productos.get(position));
                CarritoDataController.getInstance().removeProducto(productos.get(position));
                desactivarPanelCantidad(holder);
                EditText et = holder.cantidadTextView;
                et.setSelection(et.getText().length());
            }
        }

    }

    private void addProducto(CatalogoAdapter.ViewHolder holder, int position) {
        activarPanelCantidad(holder);
        int cantidad = CarritoDataController.getInstance().getCantidadProducto(productos.get(position));

        if (cantidad == 0) {
            productos.get(position).setCantidad(1);
        } else {
            productos.get(position).setCantidad(cantidad);
        }

        holder.cantidadTextView.setText(productos.get(position).getCantidad() + "");
        CarritoDataController.getInstance().addProducto(productos.get(position));
        listaFragment.actualizarProductoLista(productos.get(position));
    }

    private void minusProducto(CatalogoAdapter.ViewHolder holder, int position) {
        int cantidad = CarritoDataController.getInstance().getCantidadProducto(productos.get(position));

        if (cantidad > 0) {
            productos.get(position).setCantidad(cantidad);
            productos.get(position).setCantidad(productos.get(position).getCantidad() - 1);

            if (productos.get(position).getCantidad() > 0) {
                holder.cantidadTextView.setText(productos.get(position).getCantidad() + "");
                CarritoDataController.getInstance().addProducto(productos.get(position));
                listaFragment.actualizarProductoLista(productos.get(position));
            } else {
                productos.get(position).setCantidad(0);
                listaFragment.actualizarProductoLista(productos.get(position));
                CarritoDataController.getInstance().removeProducto(productos.get(position));
                desactivarPanelCantidad(holder);
            }
        }

    }

    private void activarPanelCantidad(CatalogoAdapter.ViewHolder holder) {
        holder.agregarButton.setVisibility(View.GONE);
        holder.cantidadLinearLayout.setVisibility(View.VISIBLE);
    }

    private void desactivarPanelCantidad(CatalogoAdapter.ViewHolder holder) {
        holder.agregarButton.setVisibility(View.VISIBLE);
        holder.cantidadLinearLayout.setVisibility(View.GONE);
    }

    private void plusProducto(CatalogoAdapter.ViewHolder holder, int position) {
        int cantidad = CarritoDataController.getInstance().getCantidadProducto(productos.get(position));

        if (cantidad > 0) {
            productos.get(position).setCantidad(cantidad);
            productos.get(position).setCantidad(productos.get(position).getCantidad() + 1);
        }

        holder.cantidadTextView.setText(productos.get(position).getCantidad() + "");
        CarritoDataController.getInstance().addProducto(productos.get(position));
        listaFragment.actualizarProductoLista(productos.get(position));
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView nombreProductoTextView, precioProductoTextView, codigoTextView, eanTextView;
        EditText cantidadTextView;
        Button agregarButton, addButton, removeButton;
        LinearLayout cantidadLinearLayout;
        ImageView productoImageView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            nombreProductoTextView = itemView.findViewById(R.id.nombreTextView);
            precioProductoTextView = itemView.findViewById(R.id.precioTextView);
            cantidadTextView = itemView.findViewById(R.id.cantidadTextView);
            agregarButton = itemView.findViewById(R.id.agregarButton);
            codigoTextView = itemView.findViewById(R.id.codigoTextView);
            eanTextView = itemView.findViewById(R.id.eanTextView);
            addButton = itemView.findViewById(R.id.addButton);
            removeButton = itemView.findViewById(R.id.removeButton);
            cantidadLinearLayout = itemView.findViewById(R.id.cantidadLinearLayout);
            productoImageView = itemView.findViewById(R.id.productoImagen);
        }
    }
}
