package com.celuweb.emart.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.celuweb.emart.DataObject.DetalleUltimoPedido;
import com.celuweb.emart.DataObject.ListaDetalle;
import com.celuweb.emart.R;
import com.celuweb.emart.Util.Util;

import java.util.ArrayList;

public class HistoricoDetallesAdapter extends RecyclerView.Adapter<HistoricoDetallesAdapter.VieHolder> {
    private ArrayList<ListaDetalle> listaDetalles;
    private ArrayList<DetalleUltimoPedido> detalleUltimoPedido;

    public void updateAdapter(ArrayList<DetalleUltimoPedido> detalleUltimoPedido, ArrayList<ListaDetalle> listaDetalles) {
        if (detalleUltimoPedido != null) {
            this.detalleUltimoPedido = detalleUltimoPedido;
        } else {
            this.listaDetalles = listaDetalles;
        }
        notifyDataSetChanged();
    }

    public HistoricoDetallesAdapter(ArrayList<DetalleUltimoPedido> detalleUltimoPedido, ArrayList<ListaDetalle> listaDetalles) {
        this.listaDetalles = listaDetalles;
        this.detalleUltimoPedido = detalleUltimoPedido;
    }

    @NonNull
    @Override
    public HistoricoDetallesAdapter.VieHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.historico_detalles_item, parent, false);
        return new HistoricoDetallesAdapter.VieHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HistoricoDetallesAdapter.VieHolder holder, int position) {
        if (detalleUltimoPedido != null) {
            holder.lblPosicion.setText("Nombre: " + detalleUltimoPedido.get(position).getNombre());
            holder.lblCantidad.setText("Precio: " + detalleUltimoPedido.get(position).getPrecio());
            holder.lblCodProducto.setText("Cantidad: " + detalleUltimoPedido.get(position).getCantidad());
        } else {
            if (listaDetalles.get(position).getIva() != 0 && listaDetalles.get(position).precio != 0) {
                holder.lblIva.setVisibility(View.VISIBLE);
                holder.lblPrecio.setVisibility(View.VISIBLE);
                holder.lblIva.setText("Iva: " + listaDetalles.get(position).getIva() + "%");
                holder.lblPrecio.setText("Precio: " + Util.getFormatCurrency((double) listaDetalles.get(position).precio));

            }
            if (listaDetalles.get(position).getNombre_producto() != null) {
                holder.lblPosicion.setText("Nombre: " + listaDetalles.get(position).getNombre_producto());
            } else {
                holder.lblPosicion.setText("Posición: " + listaDetalles.get(position).posicion);
            }
            holder.lblCantidad.setText("Cantidad: " + listaDetalles.get(position).cantidad);
            holder.lblCodProducto.setText("Cod. Producto: " + listaDetalles.get(position).getCodigo_producto());
        }
    }

    @Override
    public int getItemCount() {
        if (detalleUltimoPedido != null) {
            return detalleUltimoPedido.size();
        } else {
            return listaDetalles.size();
        }
    }

    public class VieHolder extends RecyclerView.ViewHolder {
        private TextView lblPosicion, lblCantidad, lblCodProducto, lblIva, lblPrecio;

        public VieHolder(@NonNull View itemView) {
            super(itemView);
            lblPosicion = itemView.findViewById(R.id.lbl_razon_social);
            lblCantidad = itemView.findViewById(R.id.lbl_nombre_cliente);
            lblCodProducto = itemView.findViewById(R.id.lbl_telefono);
            lblIva = itemView.findViewById(R.id.lbl_iva);
            lblPrecio = itemView.findViewById(R.id.lbl_Precio);

        }
    }
}
