package com.celuweb.emart.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.celuweb.emart.DataObject.Pedido;
import com.celuweb.emart.DataObject.UltimoPedido;
import com.celuweb.emart.R;

import java.util.ArrayList;

public class HistorialPedidosAdapter extends RecyclerView.Adapter<HistorialPedidosAdapter.VieHolder> {
    private HistoricoEvent listener;
    private ArrayList<Pedido> listaPedidos;
    private ArrayList<UltimoPedido> ultimosPedidos;


    public HistorialPedidosAdapter(ArrayList<UltimoPedido> ultimosPedidos, ArrayList<Pedido> listaPedidos, HistoricoEvent listener) {
        this.listaPedidos = listaPedidos;
        this.listener = listener;
        this.ultimosPedidos = ultimosPedidos;
    }


    @NonNull
    @Override
    public VieHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.historico_pedido_item, parent, false);
        return new VieHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull VieHolder holder, int position) {
        if (ultimosPedidos != null) {
            holder.lblNumDoc.setText("Número Documento: " + ultimosPedidos.get(position).getNumeroDoc());
            holder.lblCodigoCliente.setText("Código Cliente: " + ultimosPedidos.get(position).getCodigo());
            holder.lblFecha.setText("Fecha: " + ultimosPedidos.get(position).getFecha());
        } else {
            holder.lblNumDoc.setText("Número Documento: " + listaPedidos.get(position).getNumero_doc());
            holder.lblCodigoCliente.setText("Código Cliente: " + listaPedidos.get(position).getCodigo_cliente());
            holder.lblFecha.setText("Fecha: " + listaPedidos.get(position).getFecha_movil());
        }


    }

    @Override
    public int getItemCount() {
        if (ultimosPedidos != null) {
            return ultimosPedidos.size();
        } else {
            return listaPedidos.size();
        }
    }

    public class VieHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView lblNumDoc, lblCodigoCliente, lblFecha;
        ImageView imgDetalle;

        //private TextView
        public VieHolder(@NonNull View itemView) {
            super(itemView);
            lblNumDoc = itemView.findViewById(R.id.lbl_num_doc);
            lblCodigoCliente = itemView.findViewById(R.id.lbl_codigo_cliente);
            lblFecha = itemView.findViewById(R.id.lbl_fecha);
            imgDetalle = itemView.findViewById(R.id.img_detalle);
            imgDetalle.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            listener.OnDetailClicked(getAdapterPosition());
        }
    }


    public interface HistoricoEvent {
        void OnDetailClicked(int position);
    }


}
