package com.celuweb.emart.Adapter;


import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.celuweb.emart.DataController.CarritoDataController;
import com.celuweb.emart.DataObject.PedidoSugerido;
import com.celuweb.emart.R;

import java.util.ArrayList;

public class ListaPedidoSugeridoAdapter extends RecyclerView.Adapter<ListaPedidoSugeridoAdapter.ViewHolderPedidoSugerido> {
    private ArrayList<PedidoSugerido> listaDatos;
    private IAdapterListener listener;

    public ListaPedidoSugeridoAdapter(ArrayList<PedidoSugerido> listaDatos, IAdapterListener listener) {
        this.listaDatos = listaDatos;
        this.listener = listener;
    }

    public ArrayList<PedidoSugerido> getListaDatos() {
        return listaDatos;
    }


    public void actualizarAdapter(ArrayList<PedidoSugerido> listaDatos) {
        this.listaDatos = listaDatos;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ListaPedidoSugeridoAdapter.ViewHolderPedidoSugerido onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_row_pedido_sugerido, parent, false);
        return new ViewHolderPedidoSugerido(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderPedidoSugerido viewHolderPedidoSugerido, final int i) {
        viewHolderPedidoSugerido.descripcion.setText(listaDatos.get(i).getNombre());
        viewHolderPedidoSugerido.cantidadPed.setText("" + listaDatos.get(viewHolderPedidoSugerido.getAdapterPosition()).getCantidadPedido());
        viewHolderPedidoSugerido.cantidadSug.setText("" + listaDatos.get(i).getCantidad());
        viewHolderPedidoSugerido.diferencia.setText("" + (listaDatos.get(i).getCantidadPedido() - listaDatos.get(i).getCantidad()));
        viewHolderPedidoSugerido.codBarras.setText(listaDatos.get(i).getEan());
        viewHolderPedidoSugerido.codInterno.setText(listaDatos.get(i).getCodigo());
        viewHolderPedidoSugerido.agregar.setChecked(listaDatos.get(i).cheked);
        viewHolderPedidoSugerido.cantidadPed.setEnabled(!listaDatos.get(i).cheked);

    }


    @Override
    public int getItemCount() {
        return listaDatos.size();
    }

    public class ViewHolderPedidoSugerido extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView descripcion, cantidadSug, diferencia, unidadVenta, codBarras, codInterno;
        private EditText cantidadPed;
        private CheckBox agregar;

        public ViewHolderPedidoSugerido(@NonNull View itemView) {
            super(itemView);
            descripcion = itemView.findViewById(R.id.lbl_descripcion);
            cantidadPed = itemView.findViewById(R.id.lbl_cantidad_ped);
            cantidadSug = itemView.findViewById(R.id.lbl_cantidad_sug);
            diferencia = itemView.findViewById(R.id.lbl_diferencia);
            unidadVenta = itemView.findViewById(R.id.lbl_unidad_venta);
            codBarras = itemView.findViewById(R.id.lbl_ean);
            codInterno = itemView.findViewById(R.id.lbl_codigo);
            agregar = itemView.findViewById(R.id.check_agregar);
            agregar.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int cantidad = Integer.parseInt(cantidadPed.getText().toString());
            if (cantidad == 0) {
                cantidad = Integer.parseInt(cantidadSug.getText().toString());
            }

            listener.onMessageRowClicked(getAdapterPosition(), ((CheckBox) v).isChecked(), cantidad);
        }
    }


    public interface IAdapterListener {
        void onMessageRowClicked(int position, boolean isChecked, int cantidad);

        void onTextViewChange(int position, boolean isChecked, PedidoSugerido item);

    }

    public IAdapterListener getListener() {
        return listener;
    }

    public void setListener(IAdapterListener listener) {
        this.listener = listener;
    }


}
