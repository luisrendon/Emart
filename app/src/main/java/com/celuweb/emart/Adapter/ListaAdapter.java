package com.celuweb.emart.Adapter;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.celuweb.emart.DataController.CarritoDataController;
import com.celuweb.emart.DataObject.Producto;
import com.celuweb.emart.Fragment.CarritoFragment;
import com.celuweb.emart.Fragment.ListaFragment;
import com.celuweb.emart.R;
import com.celuweb.emart.Util.Util;


import java.text.NumberFormat;
import java.util.ArrayList;

public class ListaAdapter extends RecyclerView.Adapter<ListaAdapter.ViewHolder> {
    public ArrayList<Producto> productos;
    public ListaFragment listaFragment;

    public ListaAdapter(ArrayList<Producto> productos, ListaFragment listaFragment) {
        this.productos = productos;
        this.listaFragment = listaFragment;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.lista_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        productos.get(position).setCantidad(CarritoDataController.getInstance().getCantidadProducto(productos.get(position)));

        Log.d("CANTIDAD", productos.get(position).getCantidad() + "");
        holder.eanTextView.setText("Código de Barras: " + productos.get(position).getEan());
        holder.codigoTextView.setText("Código Interno: " + productos.get(position).getCodigo());
        holder.nombreProductoTextView.setText(productos.get(position).getNombre());
        holder.precioProductoTextView.setText(Util.getFormatCurrency(Double.parseDouble(productos.get(position).getPrecio())));

        if (productos.get(position).getCantidad() > 0) {
            activarPanelCantidad(holder);
            holder.cantidadTextView.setText(productos.get(position).getCantidad() + "");
        } else {
            desactivarPanelCantidad(holder);
            holder.cantidadTextView.setText(0 + "");
        }
        holder.cantidadTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                numberProducto(holder, position);
            }
        });

        holder.agregarButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addProducto(holder, position);
            }
        });

        holder.addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                plusProducto(holder, position);
            }
        });

        holder.removeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                minusProducto(holder, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return productos.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private void resetCell(ViewHolder holder) {
        desactivarPanelCantidad(holder);
    }


    private void numberProducto(ViewHolder holder, int position) {
        int cantidad = CarritoDataController.getInstance().getCantidadProducto(productos.get(position));

        int editText = 0;
        try {
            editText = Integer.parseInt(holder.cantidadTextView.getText().toString());
        } catch (NumberFormatException e) {
            editText = 0;
        }


        if (cantidad > 0) {
            productos.get(position).setCantidad(cantidad);
            productos.get(position).setCantidad(editText);

            if (productos.get(position).getCantidad() > 0) {
                holder.cantidadTextView.setText(productos.get(position).getCantidad() + "");
                CarritoDataController.getInstance().addProducto(productos.get(position));
                listaFragment.actualizarProductoLista(productos.get(position));
                EditText et = holder.cantidadTextView;
                et.setSelection(et.getText().length());
            } else {
                productos.get(position).setCantidad(0);
                listaFragment.actualizarProductoLista(productos.get(position));
                CarritoDataController.getInstance().removeProducto(productos.get(position));
                desactivarPanelCantidad(holder);
                EditText et = holder.cantidadTextView;
                et.setSelection(et.getText().length());
            }
        }
    }


    private void addProducto(ViewHolder holder, int position) {
        activarPanelCantidad(holder);
        int cantidad = CarritoDataController.getInstance().getCantidadProducto(productos.get(position));

        if (cantidad == 0) {
            productos.get(position).setCantidad(1);
        } else {
            productos.get(position).setCantidad(cantidad);
        }

        holder.cantidadTextView.setText(productos.get(position).getCantidad() + "");
        CarritoDataController.getInstance().addProducto(productos.get(position));
        listaFragment.actualizarProductoLista(productos.get(position));
    }

    private void minusProducto(ViewHolder holder, int position) {
        int cantidad = CarritoDataController.getInstance().getCantidadProducto(productos.get(position));

        if (cantidad > 0) {
            productos.get(position).setCantidad(cantidad);
            productos.get(position).setCantidad(productos.get(position).getCantidad() - 1);

            if (productos.get(position).getCantidad() > 0) {
                holder.cantidadTextView.setText(productos.get(position).getCantidad() + "");
                CarritoDataController.getInstance().addProducto(productos.get(position));
                listaFragment.actualizarProductoLista(productos.get(position));
            } else {
                productos.get(position).setCantidad(0);
                listaFragment.actualizarProductoLista(productos.get(position));
                CarritoDataController.getInstance().removeProducto(productos.get(position));
                desactivarPanelCantidad(holder);
            }
        }

    }

    private void plusProducto(ViewHolder holder, int position) {
        int cantidad = CarritoDataController.getInstance().getCantidadProducto(productos.get(position));

        if (cantidad > 0) {
            productos.get(position).setCantidad(cantidad);
            productos.get(position).setCantidad(productos.get(position).getCantidad() + 1);
        }

        holder.cantidadTextView.setText(productos.get(position).getCantidad() + "");
        CarritoDataController.getInstance().addProducto(productos.get(position));
        listaFragment.actualizarProductoLista(productos.get(position));
    }

    private void activarPanelCantidad(ViewHolder holder) {
        holder.agregarButton.setVisibility(View.GONE);
        holder.cantidadLinearLayout.setVisibility(View.VISIBLE);
    }

    private void desactivarPanelCantidad(ViewHolder holder) {
        holder.agregarButton.setVisibility(View.VISIBLE);
        holder.cantidadLinearLayout.setVisibility(View.GONE);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView nombreProductoTextView, precioProductoTextView, codigoTextView, eanTextView;
        EditText cantidadTextView;
        Button agregarButton, addButton, removeButton;
        LinearLayout cantidadLinearLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            nombreProductoTextView = itemView.findViewById(R.id.nombreTextView);
            precioProductoTextView = itemView.findViewById(R.id.precioTextView);
            cantidadTextView = itemView.findViewById(R.id.cantidadTextView);
            codigoTextView = itemView.findViewById(R.id.codigoTextView);
            eanTextView = itemView.findViewById(R.id.eanTextView);
            agregarButton = itemView.findViewById(R.id.agregarButton);
            addButton = itemView.findViewById(R.id.addButton);
            removeButton = itemView.findViewById(R.id.removeButton);
            cantidadLinearLayout = itemView.findViewById(R.id.cantidadLinearLayout);
        }


    }

}
