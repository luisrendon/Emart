package com.celuweb.emart.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.celuweb.emart.DataObject.Cartera;
import com.celuweb.emart.R;

import java.util.ArrayList;


public class CarteraAdapter extends RecyclerView.Adapter<CarteraAdapter.ViewHolder>
{
    public ArrayList<Cartera> carteras;

    public CarteraAdapter(ArrayList<Cartera> carteras)
    {
        this.carteras = carteras;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recyclerview_cartera, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i)
    {
        viewHolder.numeroDocumentoTextView.setText(carteras.get(i).getDocumento());
        viewHolder.condicionPagoTextView.setText(carteras.get(i).getConcepto());
        viewHolder.fechaVTextView.setText(carteras.get(i).getFecha_vencimiento());
        viewHolder.valorTextView.setText(carteras.get(i).getSaldo() + "");

        // hacer suma total cuando los valores del saldo este completos
    }

    @Override
    public int getItemCount()
    {
        return carteras.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        private TextView numeroDocumentoTextView, condicionPagoTextView, fechaVTextView, valorTextView;

        public ViewHolder(@NonNull View itemView)
        {
            super(itemView);
            numeroDocumentoTextView = itemView.findViewById(R.id.numeroDocumentoTextView);
            condicionPagoTextView = itemView.findViewById(R.id.condicionPagoTextView);
            fechaVTextView = itemView.findViewById(R.id.fechaVencimientoTextView);
            valorTextView = itemView.findViewById(R.id.valorTextView);
        }
    }
}
