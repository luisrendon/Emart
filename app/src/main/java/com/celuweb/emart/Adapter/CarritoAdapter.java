package com.celuweb.emart.Adapter;

import android.app.Dialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.celuweb.emart.DataObject.Producto;
import com.celuweb.emart.DataObject.UltimoPedido;
import com.celuweb.emart.R;
import com.celuweb.emart.Util.Util;

import java.util.ArrayList;

public class CarritoAdapter extends RecyclerView.Adapter<CarritoAdapter.VieHolder> {

    private ArrayList<Producto> listaProductos;
    private IAdapterListener listener;
    private int editText;

    public CarritoAdapter(ArrayList<Producto> listaProductos, IAdapterListener listener) {
        this.listaProductos = listaProductos;
        this.listener = listener;
    }

    public ArrayList<Producto> getListaProductos() {
        return listaProductos;
    }

    public void setListaProductos(ArrayList<Producto> listaProductos) {
        this.listaProductos = listaProductos;
    }

    @Override
    public VieHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.carrito_item, parent, false);
        return new VieHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull VieHolder holder, int position) {
        holder.titulo.setText(listaProductos.get(position).getNombre());
        holder.ean.setText("Cod. Barras: " + listaProductos.get(position).getEan());
        holder.precioCantidad.setText("Precio regular " + Util.getFormatCurrency(Double.parseDouble(listaProductos.get(position).getPrecio())) + " x " + listaProductos.get(position).getCantidad());
        holder.total.setText("" + Util.getFormatCurrency((listaProductos.get(position).getCantidad() * Double.parseDouble(listaProductos.get(position).getPrecio()))));
        holder.codInterno.setText("Cod. Interno: " + listaProductos.get(position).getCodigo());
        holder.editextCantidad.setText("" + listaProductos.get(position).getCantidad());

//        holder.total.setText("" + Util.getFormatCurrency((listaProductos.get(position).getCantidad() * Double.parseDouble(listaProductos.get(position).getPrecio()))));


        holder.editextCantidad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    editText = Integer.parseInt(holder.editextCantidad.getText().toString());
                } catch (NumberFormatException e) {
                    editText = 0;
                }

                listener.numberProducto(position, editText);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listaProductos.size();
    }

    public class VieHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView titulo, ean, precioCantidad, total, codInterno;
        private EditText editextCantidad;
        private ImageView btnMas, btnMenos;

        public VieHolder(@NonNull View itemView) {
            super(itemView);
            titulo = itemView.findViewById(R.id.lbl_titulo);
            ean = itemView.findViewById(R.id.lbl_ean);
            codInterno = itemView.findViewById(R.id.lbl_codigo_interno);
            precioCantidad = itemView.findViewById(R.id.lbl_precio_Cantidad);
            total = itemView.findViewById(R.id.lbl_total);
            btnMas = itemView.findViewById(R.id.btn_mas);
            btnMenos = itemView.findViewById(R.id.btn_menos);

            editextCantidad = itemView.findViewById(R.id.editextCantidad);


            btnMas.setOnClickListener(this);
            btnMenos.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btn_mas:
                    listener.onPlusClicked(getAdapterPosition());
                    break;
                case R.id.btn_menos:
                    listener.onMinusClicked(getAdapterPosition());
                    break;
            }
        }

    }


    public interface IAdapterListener {
        void onMinusClicked(int position);

        void onPlusClicked(int position);

        void numberProducto(int position, int cantidad2);
    }


}
