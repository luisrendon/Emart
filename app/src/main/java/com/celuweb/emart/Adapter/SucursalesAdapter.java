package com.celuweb.emart.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.celuweb.emart.DataObject.Sucursal;
import com.celuweb.emart.R;

import java.util.ArrayList;

public class SucursalesAdapter extends RecyclerView.Adapter<SucursalesAdapter.VieHolder> {
    public ArrayList<Sucursal> listaSucursales;
    private IAdapterSucursales listener;

    public SucursalesAdapter(ArrayList<Sucursal> listaSucursales, IAdapterSucursales listener) {
        this.listaSucursales = listaSucursales;
        this.listener = listener;
    }

    @NonNull
    @Override
    public SucursalesAdapter.VieHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sucursales, parent, false);
        return new VieHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SucursalesAdapter.VieHolder holder, int position) {
        holder.lblRazonSocial.setText(listaSucursales.get(position).getRazon_social());
        holder.lblNombre.setText("Nombre: " + listaSucursales.get(position).getNombre());
        holder.lblTelefono.setText("Télefono: " + listaSucursales.get(position).getTelefono());
        holder.lblDireccion.setText("Dirección: " + listaSucursales.get(position).getDireccion());
        holder.lblCiudad.setText("Ciudad: " + listaSucursales.get(position).getCiudad());
    }

    @Override
    public int getItemCount() {
        return listaSucursales.size();
    }

    public class VieHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView lblRazonSocial, lblNombre, lblTelefono, lblDireccion, lblCiudad;
        ConstraintLayout constraintLayout;

        public VieHolder(@NonNull View itemView) {
            super(itemView);
            lblRazonSocial = itemView.findViewById(R.id.lbl_razon_social);
            lblNombre = itemView.findViewById(R.id.lbl_nombre_cliente);
            lblTelefono = itemView.findViewById(R.id.lbl_telefono);
            lblCiudad = itemView.findViewById(R.id.lbl_ciudad);
            lblDireccion = itemView.findViewById(R.id.lbl_direccion);
            constraintLayout = itemView.findViewById(R.id.layout_item);
            constraintLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.onItemClicked(getAdapterPosition());
        }
    }

    public interface IAdapterSucursales {
        void onItemClicked(int position);

    }
}
