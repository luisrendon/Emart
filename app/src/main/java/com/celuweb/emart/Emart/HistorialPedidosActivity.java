package com.celuweb.emart.Emart;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;

import com.celuweb.emart.Adapter.HistorialPedidosAdapter;
import com.celuweb.emart.Adapter.HistoricoDetallesAdapter;
import com.celuweb.emart.DataController.DataController;
import com.celuweb.emart.DataObject.ListaDetalle;
import com.celuweb.emart.R;

import java.util.ArrayList;

public class HistorialPedidosActivity extends AppCompatActivity implements HistorialPedidosAdapter.HistoricoEvent {
    private RecyclerView recyclerPedidos;
    private RecyclerView recylerDetalles;
    private HistoricoDetallesAdapter adapterDetalle;
    private HistorialPedidosAdapter adapterPedido;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historial_pedidos);
        setupToolbar();
        initComponents();

    }


    public void initComponents() {
        setupToolbar();
        recyclerPedidos = findViewById(R.id.recycler_encabezado);
        recylerDetalles = findViewById(R.id.recycler_detalles);
        recylerDetalles.setLayoutManager(new LinearLayoutManager(HistorialPedidosActivity.this, LinearLayoutManager.VERTICAL, false));
        recyclerPedidos.setLayoutManager(new LinearLayoutManager(HistorialPedidosActivity.this, LinearLayoutManager.VERTICAL, false));
        adapterPedido = new HistorialPedidosAdapter(null, DataController.getInstance().getPedidosDia(), this);
        adapterDetalle = new HistoricoDetallesAdapter(null, new ArrayList<>());
        recyclerPedidos.setAdapter(adapterPedido);
        recylerDetalles.setAdapter(adapterDetalle);
    }


    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationOnClickListener(view -> {
                onBackPressed();
            });
        }
        this.getSupportActionBar().setTitle("Historial de Pedidos");
    }

    @Override
    public void OnDetailClicked(int position) {
        ArrayList<ListaDetalle> detalles = DataController.getInstance().getPedidosDia().get(position).getDetalle();
        getSupportActionBar().setTitle("Detalles pedido");
        recyclerPedidos.setVisibility(View.GONE);
        adapterDetalle.updateAdapter(null, detalles);
        recylerDetalles.setVisibility(View.VISIBLE);
    }

    @Override
    public void onBackPressed() {

        if (recylerDetalles.getVisibility() == View.VISIBLE) {
            recyclerPedidos.setVisibility(View.VISIBLE);
            recylerDetalles.setVisibility(View.GONE);
            this.getSupportActionBar().setTitle("Historial de Pedidos");
        } else {
            super.onBackPressed();
        }
    }
}
