package com.celuweb.emart.Emart;

import android.content.Intent;
import android.os.SystemClock;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.celuweb.emart.Adapter.ActivarAdapter;
import com.celuweb.emart.DataObject.Activar;
import com.celuweb.emart.DataObject.ActivarCuenta;
import com.celuweb.emart.DataObject.Empresa;
import com.celuweb.emart.DataObject.ListaDetalle;
import com.celuweb.emart.DataObject.Validar;
import com.celuweb.emart.DataObject.envioSMS;
import com.celuweb.emart.Fragment.CarritoFragment;
import com.celuweb.emart.Networking.APIs;
import com.celuweb.emart.Networking.Networking;
import com.celuweb.emart.R;
import com.celuweb.emart.DataController.DataController;
import com.celuweb.emart.Util.ProgressView;
import com.celuweb.emart.Util.ProgressView2;
import com.celuweb.emart.Util.Util;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;

import com.celuweb.emart.DataObject.Proveedor;
import com.google.gson.Gson;

import java.lang.reflect.Type;
import java.util.List;

import com.google.gson.reflect.TypeToken;


public class LoginActivity extends AppCompatActivity implements ActivarAdapter.ActivarEvent,  View.OnClickListener {
    private EditText usuarioEditText, passEditText;
    private Button loginButton;
    private TextView validarButton;
    private RecyclerView recyclerView;
    public JSONArray telefono;
    private String codigo;
    private AlertDialog alertDialog;
    private long lastClickTime = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        InitUI();
    }

    private void InitUI() {
        //DataController.getInstance().LoadFromSharedPrefs(LoginActivity.this);
        //Util.customLog("Lastlogin"+DataController.getInstance().lastLogin);

        usuarioEditText = findViewById(R.id.usuarioEditText);

        loginButton = findViewById(R.id.loginButton);
        loginButton.setOnClickListener(this);

        validarButton = findViewById(R.id.validarButton);
        validarButton.setOnClickListener(this);

        Util.permisosApp(LoginActivity.this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.loginButton:
                if (SystemClock.elapsedRealtime() - lastClickTime < 1500)
                    return;
                lastClickTime = SystemClock.elapsedRealtime();
                ProgressView.getInstance().Show(LoginActivity.this, new ProgressView.ICallback() {
                    @Override
                    public void run() {
                        login();     // para validar borrar este... tellez
//                        confirmarCuenta(); // para validar la cuenta borrar el login de arriba ... tellez
                    }
                });
                break;

            case R.id.validarButton:
                if (SystemClock.elapsedRealtime() - lastClickTime < 1500)
                    return;
                lastClickTime = SystemClock.elapsedRealtime();
                if (DataController.getInstance().getCodigo() == "" || DataController.getInstance().getCodigo() == null) {
                    Toast.makeText(getApplicationContext(),"No Existe codigo para validar",Toast.LENGTH_SHORT).show();
                }else{
                    showValidarDialog();
                }
//                ProgressView.getInstance().Show(LoginActivity.this, new ProgressView.ICallback() {
//                    @Override
//                    public void run() {
//
//                    }
//                });
                break;
        }
    }
    private void showValidarDialog() {
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(LoginActivity.this).inflate(R.layout.dialogo_validar_sms, viewGroup, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
        builder.setView(dialogView);

        Button activarCuenta = dialogView.findViewById(R.id.activarCuenta);
        EditText codigoVerificacion = dialogView.findViewById(R.id.codigoVerificacion);

//        activarCuenta.setOnClickListener(this);
        activarCuenta.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String codigoVerificado = codigoVerificacion.getText().toString();
                if(codigoVerificado ==""){
                    Toast.makeText(getApplicationContext(),"El Codigo de Verificación no puede estar vacio",Toast.LENGTH_SHORT).show();
//                    finish();
                }else{
                    activarCuenta(codigoVerificado);
//                    finish();
                }



            }
        });

        alertDialog = builder.create();
        alertDialog.show();
    }

    public boolean validar() {
        Boolean flag = true;
        if (usuarioEditText.getText().toString().equals("")) {
            Util.showDialog(LoginActivity.this, "Alerta", "Campo NIT vacio", "Aceptar", "", true, false, null, null);
            flag = false;
        }

        else if (passEditText.getText().length() < 3){
            Util.showDialog(LoginActivity.this, "Alerta", "Contraseña debe tener mas de 5 atributos", "Aceptar", "", true, false, null, null);
            flag = false;
        }
        else if(usuarioEditText.getText().toString().length()<5){
            Util.showDialog(LoginActivity.this, "Alerta", "El NIT debe contener mínimo 5 caracteres", "Aceptar", "", true, false, null, null);
            flag = false;
        }
        return flag;
    }

    private void login() {
        if (!Util.checkInternet(LoginActivity.this)) {
            mostrarAlerta("No hay conexion a internet");
            ProgressView.getInstance().Dismiss();
            return;
        }
            String nit = usuarioEditText.getText().toString();
            String url = APIs.PROVEEDORES.concat("nit=").concat(nit);
            Networking.getInstance().get(url, new Networking.ICallback() {
                @Override
                public void onFail(String error) {
                    Log.i("wsonFailure", error);
                    ProgressView.getInstance().Dismiss();
                }

                @Override
                public void onSuccess(String response) {

                    Log.i("wsonResponse", response);
                    ArrayList<Empresa> listaEmpresa = new ArrayList<>();
                    // ArrayList<Proveedor> listaProveedores = new ArrayList<>();
                    try {

                        JSONArray array = new JSONArray(response);

                        Type arrayType = new TypeToken<ArrayList<Empresa>>() {
                        }.getType();

                        // listaProveedores = new Gson().fromJson(array.toString(), arrayType);
                        listaEmpresa = new Gson().fromJson(array.toString(), arrayType);

                        if (listaEmpresa.size() > 0) {

                            //DataController.getInstance().setListaProveedores(listaProveedores);
                            DataController.getInstance().setListaEmpresas(listaEmpresa);
                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                            startActivity(intent);
                            ProgressView.getInstance().Dismiss();
                            finish();

                        } else {

                            Util.showDialog(LoginActivity.this, "Alerta", "Sin Proveedores", "Aceptar", "", true, false, null, null);

                        }
                    } catch (JSONException e) {
                        //TODO AVISAR AL USUARIO QUE EL MÉTDO FALLÓ
                        e.printStackTrace();
                    }
                }
            });
    }

    private void confirmarCuenta()
    {
        if (!Util.checkInternet(LoginActivity.this)) {
            mostrarAlerta("No hay conexion a internet");
            return;
        }
        if (validar()) {

            String Nit = usuarioEditText.getText().toString();
            String DeviceId = "12345678900987456123";
            String DeviceType = "Android";

            Validar validar = new Validar(Nit,DeviceId,DeviceType);

            String json = new Gson().toJson(validar);
            Networking.postJSON(APIs.VALIDAR, json, new Callback()
            {
                @Override
                public void onFailure(Call call, IOException e)
                {
                    LoginActivity.this.runOnUiThread(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            Toast.makeText(getApplicationContext(),"Verifique el numero de telefono o \n Su Conexion a la Red",Toast.LENGTH_SHORT).show();
                            ProgressView.getInstance().Dismiss();

                        }
                    });
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException
                {
                    try
                    {
                        JSONObject jsonObject = new JSONObject(response.body().string());

                        String mensaje = jsonObject.getString("Activo");
                        codigo = jsonObject.getString("Codigo");
                        telefono = jsonObject.getJSONArray("Telefonos");

                        ArrayList<String> list = new ArrayList<String>();
                        if (telefono != null) {// parada para castear toda la info de un jsonObject a un arraylist
                            int len = telefono.length();
                            for (int i=0;i<len;i++){
                                list.add(telefono.get(i).toString());
                            }
                        }
                        if (mensaje.equals("0"))
                        {
                            final Activar activar = new Gson().fromJson(json.toString(), Activar.class);

                            LoginActivity.this.runOnUiThread(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    showCustomDialog(list, codigo);
                                }
                            });
                        }
                        else
                        {
//                            LoginActivity.this.runOnUiThread(new Runnable()
//                            {
//                                @Override
//                                public void run()
//                                {
//                                    Util.showDialog(LoginActivity.this, "Alerta", "Verifique el codigo de confirmacion", "Aceptar", "", true, false, null, null);
//                                                                                showCustomDialog();
//                                }
//                            });
                            login();
                        }

                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                    }
                }
            });
        }

    }

    private void enviarMensajeSMS(String numero)
    {
        if (!Util.checkInternet(LoginActivity.this)) {
            mostrarAlerta("No hay conexion a internet");
            return;
        }

        int Codigo = Integer.parseInt(codigo);
//        String Telefono = numero;
        String Telefono = "3122609820";

        envioSMS envio = new envioSMS(Codigo,Telefono);

        String json = new Gson().toJson(envio);
        Networking.postJSON(APIs.ENVIOSMS, json, new Callback()
        {
            @Override
            public void onFailure(Call call, IOException e)
            {
                LoginActivity.this.runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        Toast.makeText(getApplicationContext(),"Verifique la conexion a la red",Toast.LENGTH_SHORT).show();
                        ProgressView.getInstance().Dismiss();
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException
            {
                try
                {
                    JSONObject jsonObject = new JSONObject(response.body().string());

                    String mensaje = jsonObject.getString("Messaje");
                    String Estado = jsonObject.getString("Estado");

                    if (Estado.equals("OK"))
                    {
                        LoginActivity.this.runOnUiThread(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                Util.showDialog(LoginActivity.this, "Enviado", mensaje, "Aceptar", "", true, false, null, null);
                                DataController.getInstance().setCodigo(codigo);
//                                DataController.getInstance().SaveToSharedPrefs(LoginActivity.this);
                                alertDialog.dismiss();
                                ProgressView.getInstance().Dismiss();
                            }
                        });
                    }
                    else
                    {
                        LoginActivity.this.runOnUiThread(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                Util.showDialog(LoginActivity.this, "Alerta", "Verifique el envio de datos", "Aceptar", "", true, false, null, null);
                                alertDialog.dismiss();
                                ProgressView.getInstance().Dismiss();
                            }
                        });
                    }

                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        });


    }
    private void activarCuenta(String codigoSMS) {
        if (!Util.checkInternet(LoginActivity.this)) {
            mostrarAlerta("No hay conexion a internet");
            return;
        }
        if (codigoSMS == "" || codigoSMS == null) {

            Util.showDialog(LoginActivity.this, "Alerta", "Campo de verificacion no puede estar vacio", "Aceptar", "", true, false, null, null);

        } else {

            int Codigo = Integer.parseInt(DataController.getInstance().getCodigo());
            String CodigoVerficacion = codigoSMS;

            ActivarCuenta envio = new ActivarCuenta(Codigo,CodigoVerficacion);

            String json = new Gson().toJson(envio);
            Networking.postJSON(APIs.ACTIVAR, json, new Callback()
            {
                @Override
                public void onFailure(Call call, IOException e)
                {
                    LoginActivity.this.runOnUiThread(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            Util.showDialog(LoginActivity.this, "Alerta", "Verifique el codigo e intente nuevamente", "Aceptar", "", true, false, null, null);

                        }
                    });
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException
                {
                    try
                    {
                        JSONObject jsonObject = new JSONObject(response.body().string());


                        String mensajeMalo = jsonObject.getString("Message");
                        if (mensajeMalo.equals("The request is invalid."))
                        {
                            LoginActivity.this.runOnUiThread(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    Util.showDialog(LoginActivity.this, "Error", "Verifique el codigo de verificacion", "Aceptar", "", true, false, null, null);
                                    alertDialog.dismiss();
                                    ProgressView.getInstance().Dismiss();
                                }
                            });
                        }else{
                            String mensaje = jsonObject.getString("Mensaje");
                            String Activado = jsonObject.getString("Activado");
                            if (Activado.equals("1"))
                            {
                                LoginActivity.this.runOnUiThread(new Runnable()
                                {
                                    @Override
                                    public void run()
                                    {
                                        Util.showDialog(LoginActivity.this, "Activado", mensaje, "Aceptar", "", true, false, null, null);
                                        alertDialog.dismiss();
                                        ProgressView.getInstance().Dismiss();
                                    }
                                });
                            }
                            else
                            {
                                LoginActivity.this.runOnUiThread(new Runnable()
                                {
                                    @Override
                                    public void run()
                                    {
                                        Util.showDialog(LoginActivity.this, "Error", "Verifique el codigo de verificacion", "Aceptar", "", true, false, null, null);
                                        alertDialog.dismiss();
                                        ProgressView.getInstance().Dismiss();
                                    }
                                });
                            }
                        }



                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                    }
                }
            });




        }

    } // verificar el servico por edison ... esta un numero de telefono quemado


    private void showCustomDialog(ArrayList<String> list, String codigo) {
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(LoginActivity.this).inflate(R.layout.dialogo_activar, viewGroup, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
        builder.setView(dialogView);

//        Button aceptarDialogo = dialogView.findViewById(R.id.btn_aceptar_activar);

        recyclerView = dialogView.findViewById(R.id.recycler_activar);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        ActivarAdapter rvAdapter = new ActivarAdapter(list,this);
        recyclerView.setAdapter(rvAdapter);

//        aceptarDialogo.setOnClickListener(this);


        alertDialog = builder.create();
        alertDialog.show();
        }
//        else ProgressView.getInstance().Dismiss();
//    }

    private void mostrarAlerta(final String msj) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Util.showDialog(LoginActivity.this, "Alerta", msj, "Aceptar", "", true, false, null, null);
            }
        });
    }

    @Override
    public void enviarSMS(String numero) {

        enviarMensajeSMS(numero);
    }


}
