package com.celuweb.emart.Emart;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.celuweb.emart.Adapter.SucursalesAdapter;
import com.celuweb.emart.DataController.DataController;
import com.celuweb.emart.DataObject.Sucursal;
import com.celuweb.emart.R;

import java.util.ArrayList;

public class SucursalActivity extends AppCompatActivity implements SucursalesAdapter.IAdapterSucursales, View.OnClickListener {
    private RecyclerView sucursales;
    private SucursalesAdapter adapter;
    private EditText txt_busqueda;
    private ImageView btn_limpiar;
    private ArrayList<Sucursal> arraySucursales = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sucursal);
        setupToolbar();
        initComponents();
    }

    public void initComponents() {
        sucursales = findViewById(R.id.recyclerScursales);
        txt_busqueda = findViewById(R.id.txt_busquedaSucursal);
        btn_limpiar = findViewById(R.id.btn_limpiarSucursal);
        btn_limpiar.setOnClickListener(this);

        sucursales.setLayoutManager(new LinearLayoutManager(SucursalActivity.this, LinearLayoutManager.VERTICAL, false));
        Bundle bundle = getIntent().getExtras();
        arraySucursales = DataController.getInstance().getSurcursalesSelec();
        cargarRecycler("");

        txt_busqueda.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //System.out.println(s.toString() + " " + start + " " + count + " " + after);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //System.out.println(s.toString() + " " + start + " " + count);
            }

            @Override
            public void afterTextChanged(Editable s) {
                cargarRecycler(txt_busqueda.getText().toString());
            }
        });
    }

    @Override
    public void onItemClicked(int position) {
        Intent intent = new Intent(SucursalActivity.this, MainActivity.class);
        intent.putExtra("SUCURSAL_RESULT", position);
        setResult(SucursalActivity.RESULT_OK, intent);
        finish();

    }

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationOnClickListener(view -> {
                onBackPressed();
            });
        }
        this.getSupportActionBar().setTitle("Seleccionar Sucursal");
    }

    private ArrayList<Sucursal> consultarSucursales(String filtro){
        String busqueda = filtro.toUpperCase();
        ArrayList<Sucursal> filtradas = new ArrayList<>();
        if(!busqueda.equals("")) {
            for (Sucursal sucursal : arraySucursales) {
                if (sucursal.getCiudad().contains(busqueda) || sucursal.getRazon_social().contains(busqueda) || sucursal.getNombre().contains(busqueda)) {
                    filtradas.add(sucursal);
                }
            }
            btn_limpiar.setVisibility(View.VISIBLE);
            return filtradas;
        }else{
            btn_limpiar.setVisibility(View.GONE);
            return arraySucursales;
        }
    }

    private void cargarRecycler(String filtro){
        adapter = new SucursalesAdapter(consultarSucursales(filtro), this);
        sucursales.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_limpiarSucursal:
                txt_busqueda.setText("");
                btn_limpiar.setVisibility(View.GONE);
                break;
        }
    }
}
