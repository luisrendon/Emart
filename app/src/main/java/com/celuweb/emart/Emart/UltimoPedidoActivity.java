package com.celuweb.emart.Emart;


import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.celuweb.emart.Adapter.HistorialPedidosAdapter;
import com.celuweb.emart.Adapter.HistoricoDetallesAdapter;
import com.celuweb.emart.Adapter.UltimoPedidoAdapter;
import com.celuweb.emart.DataController.DataController;
import com.celuweb.emart.DataObject.DetalleUltimoPedido;
import com.celuweb.emart.DataObject.ListaDetalle;
import com.celuweb.emart.DataObject.Proveedor;
import com.celuweb.emart.DataObject.UltimoPedido;
import com.celuweb.emart.Fragment.PedidoSugeridoFragment;
import com.celuweb.emart.Networking.APIs;
import com.celuweb.emart.Networking.Networking;
import com.celuweb.emart.R;
import com.celuweb.emart.Util.ProgressView;
import com.celuweb.emart.Util.ProgressView2;
import com.celuweb.emart.Util.Util;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.concurrent.Callable;

public class UltimoPedidoActivity extends AppCompatActivity implements UltimoPedidoAdapter.ItemClickListener, HistorialPedidosAdapter.HistoricoEvent {

    ArrayList<UltimoPedido> listadoUltimosPedidos;
    private RecyclerView recyclerViewEncabezado;
    private RecyclerView recyclerDetalle;
    private HistoricoDetallesAdapter detallesAdapter;
    private HistorialPedidosAdapter encabezadoAdapter;

    UltimoPedido ultimoPedidoSeleccionado;
    Dialog dialogoDetallesUltimoPedido;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ultimo_pedido);

        setupToolbar();
        initUI();

    }


    @Override
    protected void onResume() {
        super.onResume();
    }


    public void consultarUltimosPedidos() {
        if (!Util.checkInternet(UltimoPedidoActivity.this)) {
            Util.showDialog(UltimoPedidoActivity.this, "Alerta", "No Hay conexión a internet", "Aceptar", "", true, false, null, null);
            ProgressView.getInstance().Dismiss();
            return;
        }
        Proveedor proveedorselect = DataController.getInstance().getProveedor();
        String params = "?origen=" + proveedorselect.getCodigo_empresa() + "&cliente=" + proveedorselect.getCodigo();

        String url = Util.getUrl(params, proveedorselect, APIs.PEDIDO_END);

        ProgressView2.show(this);

        Networking.get(url, new Networking.ICallback() {
            @Override
            public void onFail(String error) {

                ProgressView2.dismiss();
                Util.showDialog(UltimoPedidoActivity.this, "Alerta", "No se pudo descargar la lista de ultimos pedidos", "Aceptar", "", true, false, null, null);
            }

            @Override
            public void onSuccess(String response) {

                try {


                    JSONArray jsonArray = new JSONArray(response);
                    Type arrayType = new TypeToken<ArrayList<UltimoPedido>>() {
                    }.getType();
                    listadoUltimosPedidos = new Gson().fromJson(jsonArray.toString(), arrayType);
                    //DataController.getInstance().setListaProductos(listaProductos);
                    ProgressView2.dismiss();
                    encabezadoAdapter = new HistorialPedidosAdapter(listadoUltimosPedidos, null, UltimoPedidoActivity.this);
                    recyclerViewEncabezado.setAdapter(encabezadoAdapter);
                    //  cargarRecyclerView();


                } catch (JSONException e) {
                    Util.showDialog(UltimoPedidoActivity.this, "Alerta", "No se pudo descargar la lista de ultimos pedidos", "Aceptar", "", true, false, null, null);
                    e.printStackTrace();
                }

            }
        });


    }


    Callable<Void> failCallDialog = new Callable<Void>() {
        @Override
        public Void call() throws Exception {
            onBackPressed();
            return null;
        }
    };


    private void initUI() {
        recyclerViewEncabezado = findViewById(R.id.recycler_encabezado);
        recyclerViewEncabezado.setLayoutManager(new LinearLayoutManager(UltimoPedidoActivity.this, LinearLayoutManager.VERTICAL, false));
        recyclerDetalle = findViewById(R.id.recycler_detalles);
        recyclerDetalle.setLayoutManager(new LinearLayoutManager(UltimoPedidoActivity.this, LinearLayoutManager.VERTICAL, false));
        detallesAdapter = new HistoricoDetallesAdapter(null, new ArrayList<>());
        recyclerDetalle.setAdapter(detallesAdapter);
        consultarUltimosPedidos();
    }

    private void cargarRecyclerView() {
        UltimoPedidoAdapter adapter = new UltimoPedidoAdapter(this, listadoUltimosPedidos);
        adapter.setClickListener(this);
        recyclerViewEncabezado.setAdapter(adapter);
        adapter.notifyDataSetChanged();

    }


    @Override
    public void onItemClick(View view, int position) {
        //Log.i("TAG", "You clicked number " + adapter.getItem(position) + ", which is at cell position " + position);

        ultimoPedidoSeleccionado = listadoUltimosPedidos.get(position);
        mostrarVentanaDetallesPedido();

    }

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationOnClickListener(view -> {
                onBackPressed();
            });
        }
        this.getSupportActionBar().setTitle("Historial de Pedidos");
    }

    public void mostrarVentanaDetallesPedido() {


        if (dialogoDetallesUltimoPedido != null)
            if (dialogoDetallesUltimoPedido.isShowing())
                dialogoDetallesUltimoPedido.cancel();


        dialogoDetallesUltimoPedido = new Dialog(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialogoDetallesUltimoPedido.setContentView(R.layout.dialog_detalles);
        dialogoDetallesUltimoPedido.setTitle("Ultimos Pedidos");


        Button btnDeVentaSugerida = ((Button) dialogoDetallesUltimoPedido.findViewById(R.id.btnDeVentaSugerida));
        btnDeVentaSugerida.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                dialogoDetallesUltimoPedido.cancel();
                finish();


            }
        });

        dialogoDetallesUltimoPedido.setCancelable(false);
        dialogoDetallesUltimoPedido.show();


    }


    @Override
    public void OnDetailClicked(int position) {
        Toast.makeText(UltimoPedidoActivity.this, "presionó el detalle" + position, Toast.LENGTH_SHORT).show();

        ArrayList<DetalleUltimoPedido> detalles = listadoUltimosPedidos.get(position).getDetalles();
        getSupportActionBar().setTitle("Detalles últimos pedidos");
        recyclerViewEncabezado.setVisibility(View.GONE);
        detallesAdapter.updateAdapter(detalles, null);
        recyclerDetalle.setVisibility(View.VISIBLE);
    }


    public void onBackPressed() {

        if (recyclerDetalle.getVisibility() == View.VISIBLE) {
            recyclerViewEncabezado.setVisibility(View.VISIBLE);
            recyclerDetalle.setVisibility(View.GONE);
            this.getSupportActionBar().setTitle("últimos Pedidos");
        } else {
            super.onBackPressed();
        }
    }

}
