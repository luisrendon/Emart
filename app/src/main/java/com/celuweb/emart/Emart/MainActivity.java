package com.celuweb.emart.Emart;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.os.Bundle;

import com.celuweb.emart.Adapter.ListaProveedoresAdapter;
import com.celuweb.emart.DataController.DataController;
import com.celuweb.emart.DataObject.Categoria;
import com.celuweb.emart.DataObject.Empresa;
import com.celuweb.emart.DataObject.Pedido;
import com.celuweb.emart.DataObject.Producto;
import com.celuweb.emart.DataObject.Proveedor;
import com.celuweb.emart.DataObject.Sucursal;
import com.celuweb.emart.Fragment.ListaFragment;
import com.celuweb.emart.Networking.APIs;
import com.celuweb.emart.Networking.Networking;
import com.celuweb.emart.R;
import com.celuweb.emart.Util.Const;
import com.celuweb.emart.Util.ProgressView;
import com.celuweb.emart.Util.Util;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.concurrent.Callable;

import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.GridLayoutManager;

import android.os.SystemClock;
import android.view.View;
import android.view.ViewGroup;
import android.util.Log;
import android.widget.Toast;

import static com.celuweb.emart.DataController.DataController.getInstance;

public class MainActivity extends AppCompatActivity implements ListaProveedoresAdapter.ItemClickListener {

    private RecyclerView recyclerView;
    ArrayList<Proveedor> listaProveedores = new ArrayList<>();
    Proveedor proveedorSeleccionado;
    private long lastClickTime = 0;
    private ArrayList<Producto> listaProductos;
    private ArrayList<Categoria> listaCategorias;
    private ArrayList<Empresa> listaEmpresas;
    private Empresa empresaSelect;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initUI();
        cargarRecyclerView();
    }


    private void initUI() {
        recyclerView = findViewById(R.id.recyclerView);
        setupToolbar();
        //recyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this, LinearLayoutManager.VERTICAL, false));
        int numberOfColumns = 3;
        recyclerView.setLayoutManager(new GridLayoutManager(this, numberOfColumns));
    }

    private void cargarRecyclerView() {
        listaProveedores = DataController.getInstance().getListaProveedores();
        listaEmpresas = DataController.getInstance().getListaEmpresas();
        ListaProveedoresAdapter adapter = new ListaProveedoresAdapter(this, listaEmpresas);
        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }


    @Override
    public void onItemClick(View view, int position) {

        if (SystemClock.elapsedRealtime() - lastClickTime < 1500)
            return;
        lastClickTime = SystemClock.elapsedRealtime();

        empresaSelect = DataController.getInstance().getListaEmpresas().get(position);
        DataController.getInstance().setEmpresaSlect(empresaSelect);
        if (empresaSelect.getSucursales().size() > 1) {
            DataController.getInstance().setSurcursalesSelec(listaEmpresas.get(position).getSucursales());
            Intent intent = new Intent(MainActivity.this, SucursalActivity.class);
            startActivityForResult(intent, Const.SUCURSALES_ACTIVITY);
        } else {
            abrirProveedor(0, empresaSelect);
            /*proveedorSeleccionado = listaProveedores.get(position);
            DataController.getInstance().setProveedor(proveedorSeleccionado);
            consultarProductosCategorias(); //PASAR ESTE METOR*/
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Const.SUCURSALES_ACTIVITY) {
            if (data != null) {
                abrirProveedor(data.getExtras().getInt("SUCURSAL_RESULT"), DataController.getInstance().getEmpresaSlect());
            }
        }
    }

    public void abrirProveedor(int pos, Empresa empresaSelect) {
        Proveedor proveedor = new Proveedor();
        proveedor.setNombre(empresaSelect.getSucursales().get(pos).getNombre());
        proveedor.setCodigo(empresaSelect.getSucursales().get(pos).getCodigo());
        proveedor.setRazonsocial(empresaSelect.getSucursales().get(pos).getRazon_social());
        proveedor.setNum_empresa(empresaSelect.getNum_empresa());
        proveedor.setNombre_empresa(empresaSelect.getNombre_empresa());
        proveedor.setTipo(empresaSelect.getTipo());
        proveedor.setTelefono(empresaSelect.getSucursales().get(pos).getTelefono());
        proveedor.setUrl(empresaSelect.getUrl());
        proveedor.setPath(empresaSelect.getPath());
        proveedor.setImagen(empresaSelect.getImagen());
        proveedor.setCodigo_empresa(empresaSelect.getCodigo_empresa());
        proveedor.setPedido_minimo(empresaSelect.getPedido_minimo());
        proveedor.setMax_pedidos(empresaSelect.getMax_pedidos());
        DataController.getInstance().setProveedor(proveedor);
        consultarProductosCategorias();
    }

    //SERVICIO EN CARGADO DE CONSULTAR LAS CATEGORIAS DEL LOS PRODUCTOS PARA MOSTRAR LOS PRODUCTOS
    public void consultarProductosCategorias() {
        Proveedor proveedorSelec = DataController.getInstance().getProveedor();

        if (!Util.checkInternet(MainActivity.this)) {
            Util.showDialog(MainActivity.this, "Alerta", "No Hay conexión a internet", "Aceptar", "", true, false, null, null);
            ProgressView.getInstance().Dismiss();
            return;
        }

        if (proveedorSelec == null) {
            Util.showDialog(MainActivity.this, "Alerta", "No se pudo leer la información del proveedor", "Aceptar", "", true, false, null, null);
            ProgressView.getInstance().Dismiss();
            return;
        }


        String params = "?origen=" + proveedorSelec.getCodigo_empresa() + "&cliente=" + proveedorSelec.getCodigo();
        String url = Util.getUrl(params, proveedorSelec, APIs.CATEGORIA_END);

        ProgressView.getInstance().Show(this, new ProgressView.ICallback() {
            @Override
            public void run() {
                Networking.get(url, new Networking.ICallback() {
                    @Override
                    public void onFail(String error) {
                        ProgressView.getInstance().Dismiss();
                        Util.showDialog(MainActivity.this, "Alerta", "No se pudo descargar la lista de categoriss", "Aceptar", "", true, false, null, null);
                        ProgressView.getInstance().Dismiss();
                    }

                    @Override
                    public void onSuccess(String response) {

                        ProgressView.getInstance().Dismiss();
                        try {
                            Log.d("JSON", response);
                            JSONArray jsonArray = new JSONArray(response);
                            Type arrayType = new TypeToken<ArrayList<Categoria>>() {
                            }.getType();

                            if (jsonArray.length() > 0) {
                                listaCategorias = new Gson().fromJson(jsonArray.toString(), arrayType);
                                DataController.getInstance().setListaCategoria(listaCategorias);
                                consultarPedidosDia();
                            } else {
                                Util.showDialog(MainActivity.this, "Alerta", "No se pudo descargar la lista de categorias", "Aceptar", "", true, false, null, null);
                            }


                        } catch (JSONException e) {
                            Util.showDialog(MainActivity.this, "Alerta", "No se pudo descargar la lista de categorias", "Aceptar", "", true, false, null, null);
                            e.printStackTrace();
                        }

                    }
                });
            }
        });

    }


    public void consultarPedidosDia() {

        ProgressView.getInstance().Show(MainActivity.this, new ProgressView.ICallback() {
            @Override
            public void run() {

                if (!Util.checkInternet(MainActivity.this)) {
                    Util.showDialog(MainActivity.this, "Alerta", "No Hay conexión a internet", "Aceptar", "", true, false, null, null);
                    ProgressView.getInstance().Dismiss();
                    return;
                }

                Proveedor proveedorSelec = DataController.getInstance().getProveedor();
                String params = "?codigo=" + proveedorSelec.getCodigo_empresa() + "&cliente=" + proveedorSelec.getCodigo();
                String url = Util.getUrl(params, proveedorSelec, APIs.REALIZADO_END);

                ProgressView.getInstance().Show(MainActivity.this, new ProgressView.ICallback() {
                    @Override
                    public void run() {
                        Networking.get(url, new Networking.ICallback() {
                            @Override
                            public void onFail(String error) {
                                ProgressView.getInstance().Dismiss();
                                Util.showDialog(MainActivity.this, "Alerta", "No se pudo descargar la información", "Aceptar", "", true, false, null, null);
                                return;
                            }

                            @Override
                            public void onSuccess(String response) {
                                try {

                                    ProgressView.getInstance().Dismiss();
                                    JSONArray jsonArray = new JSONArray(response);
                                    Type arrayType = new TypeToken<ArrayList<Pedido>>() {
                                    }.getType();
                                    ArrayList<Pedido> pedidos = new Gson().fromJson(jsonArray.toString(), arrayType);
                                    DataController.getInstance().setPedidosDia(new Gson().fromJson(jsonArray.toString(), arrayType));
                                    Intent intent = new Intent(MainActivity.this, CatalogoActivity.class);
                                    startActivity(intent);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                });

            }
        });

    }


    @Override
    public void onBackPressed() {
        // super.onBackPressed();
        Util.showDialog(MainActivity.this, "Alerta", "Seguro que desea salir", "Aceptar", "Cancelar", true, true, new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
                return null;
            }
        }, null);
    }


    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationOnClickListener(view -> {
                onBackPressed();
            });
        }
    }


}
