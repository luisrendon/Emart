package com.celuweb.emart.Emart;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.celuweb.emart.BottomMenuHelper;
import com.celuweb.emart.DataController.CarritoDataController;
import com.celuweb.emart.DataController.DataController;
import com.celuweb.emart.DataObject.Categoria;
import com.celuweb.emart.DataObject.Producto;
import com.celuweb.emart.DataObject.Proveedor;
import com.celuweb.emart.Fragment.BuscarFragment;
import com.celuweb.emart.Fragment.CarritoFragment;
import com.celuweb.emart.Fragment.CatalogoFragment;
import com.celuweb.emart.Fragment.CatalogoFragment2;
import com.celuweb.emart.Fragment.ListaFragment;
import com.celuweb.emart.Fragment.PedidoSugeridoFragment;
import com.celuweb.emart.Networking.APIs;
import com.celuweb.emart.Networking.Networking;
import com.celuweb.emart.R;
import com.celuweb.emart.Util.Const;
import com.celuweb.emart.Util.ProgressView;
import com.celuweb.emart.Util.Util;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public class CatalogoActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private ArrayList<Producto> listaProductos;
    private ArrayList<Categoria> listaCategorias;
    private BottomNavigationView bottomNavigationView;
    private CatalogoFragment2 catalogoFragment;
    private BuscarFragment buscarFragment;
    private ListaFragment listaFragment;
    private CarritoFragment carritoFragment;
    private PedidoSugeridoFragment pedidoSugeridoFragment;
    private int switchMenu;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_catalogo);
        initComponents();
    }

    public void initComponents() {
        listaProductos = new ArrayList<>();
        catalogoFragment = new CatalogoFragment2();
        listaFragment = new ListaFragment();
        carritoFragment = new CarritoFragment();
        buscarFragment = new BuscarFragment();
        pedidoSugeridoFragment = new PedidoSugeridoFragment();
        navigationView = findViewById(R.id.drawer_navigation_view);
        navigationView.setNavigationItemSelectedListener(this);
        drawerLayout = findViewById(R.id.drawer_layout);
        setupToolbar();
        switchMenu = Const.LISTA_FRAGMENT;
        View headerLayout = navigationView.getHeaderView(0);
        ((TextView) headerLayout.findViewById(R.id.lbl_empresa)).setText(DataController.getInstance().getProveedor().getNombre_empresa());
        ((TextView) headerLayout.findViewById(R.id.lbl_nombre_usuario)).setText(DataController.getInstance().getProveedor().getNombre());
        bottomNavigationView = findViewById(R.id.bottomNavigationView);
        getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout, listaFragment).commit();
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                switch (item.getItemId()) {
                    case R.id.navigation_pedido:
                        if (switchMenu != Const.LISTA_FRAGMENT) {
                            transaction.replace(R.id.frameLayout, listaFragment);
                            limpiarFragmentos();
                            transaction.commit();
                            switchMenu = Const.LISTA_FRAGMENT;
                        }
                        return true;
                    case R.id.navigation_catalogo:
                        if (switchMenu != Const.CATALOGO_FRAGMENT) {
                            transaction.replace(R.id.frameLayout, catalogoFragment);
                            limpiarFragmentos();
                            transaction.commit();
                            switchMenu = Const.CATALOGO_FRAGMENT;
                        }
                        return true;

                    case R.id.navigation_carrito:
                        if (switchMenu != Const.CARRITO_FRAGMENT) {
                            transaction.replace(R.id.frameLayout, carritoFragment);
                            limpiarFragmentos();
                            transaction.commit();
                            switchMenu = Const.CARRITO_FRAGMENT;
                        }
                        return true;
                    case R.id.navigation_sugerido:
                        if (switchMenu != Const.SUGERIDO_FRAGMENT) {
                            transaction.replace(R.id.frameLayout, pedidoSugeridoFragment);
                            limpiarFragmentos();
                            transaction.commit();
                            switchMenu = Const.SUGERIDO_FRAGMENT;
                        }
                        return true;
                    case R.id.navigation_buscar:
                        if (switchMenu != Const.BUSCAR_PRODUCTO) {
                            transaction.replace(R.id.frameLayout, buscarFragment);
                            limpiarFragmentos();
                            transaction.commit();
                            switchMenu = Const.BUSCAR_PRODUCTO;
                        }
                        return true;

                }
                return false;
            }
        });

        //BottomMenuHelper.showBadge(this, bottomNavigationView, R.id.navigation_carrito, 5+"");
        //mostrarCantidad(5);
    }

    private void setupToolbar() {

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);
            toolbar.setNavigationOnClickListener(view -> {
                toggleDrawer();
            });
        }
    }




    public void consultarProductos() {
        Proveedor proveedorSelec = DataController.getInstance().getProveedor();
        if (!Util.checkInternet(CatalogoActivity.this)) {
            Util.showDialog(CatalogoActivity.this, "Alerta", "No Hay conexión a internet", "Aceptar", "", true, false, failCallDialog, null);
            ProgressView.getInstance().Dismiss();
            return;
        }
        if (proveedorSelec == null) {
            Util.showDialog(CatalogoActivity.this, "Alerta", "No se pudo leer la información del proveedor", "Aceptar", "", true, false, failCallDialog, null);
            ProgressView.getInstance().Dismiss();
            return;
        }
        //TODO REVISAR SI LA URL AL FIN SE CONCATENA CON EL PATH
        String url = proveedorSelec.getUrl() + proveedorSelec.getPath()
                + "?origen=" + proveedorSelec.getCodigo_empresa()
                + "&cliente=" + proveedorSelec.getCodigo();

        ProgressView.getInstance().Show(() -> {
        });

        Networking.get(url, new Networking.ICallback() {
            @Override
            public void onFail(String error) {
                ProgressView.getInstance().Dismiss();
                Util.showDialog(CatalogoActivity.this, "Alerta", "No se pudo descargar la lista de productos", "Aceptar", "", true, false, failCallDialog, null);
            }

            @Override
            public void onSuccess(String response) {

                try {
                    ProgressView.getInstance().Dismiss();
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("result");
                    Type arrayType = new TypeToken<ArrayList<Producto>>() {
                    }.getType();
                    listaProductos = new Gson().fromJson(jsonArray.toString(), arrayType);
                    DataController.getInstance().setListaProductos(listaProductos);

                } catch (JSONException e) {
                    Util.showDialog(CatalogoActivity.this, "Alerta", "No se pudo descargar la lista de productos", "Aceptar", "", true, false, failCallDialog, null);
                    e.printStackTrace();
                }

            }
        });
    }

    @Override
    public void onBackPressed() {
        ProgressView.getInstance().Dismiss();
        Intent intent = new Intent(CatalogoActivity.this, MainActivity.class);
        startActivity(intent);
        //  super.onBackPressed();

    }

    Callable<Void> failCallDialog = new Callable<Void>() {
        @Override
        public Void call() throws Exception {
            onBackPressed();
            return null;
        }
    };

    public void limpiarFragmentos() {
        List<Fragment> fragments = getSupportFragmentManager().getFragments();

        for (Fragment frament : fragments) {
            getSupportFragmentManager().beginTransaction().remove(frament).commit();
        }

    }

    //SERVICIO EN CARGADO DE CONSULTAR LAS CATEGORIAS DEL LOS PRODUCTOS PARA MOSTRAR LOS PRODUCTOS
    public void consultarProductosCategorias() {
        Proveedor proveedorSelec = DataController.getInstance().getProveedor();

        if (!Util.checkInternet(CatalogoActivity.this)) {
            Util.showDialog(CatalogoActivity.this, "Alerta", "No Hay conexión a internet", "Aceptar", "", true, false, failCallDialog, null);
            ProgressView.getInstance().Dismiss();
            return;
        }

        if (proveedorSelec == null) {
            Util.showDialog(CatalogoActivity.this, "Alerta", "No se pudo leer la información del proveedor", "Aceptar", "", true, false, failCallDialog, null);
            ProgressView.getInstance().Dismiss();
            return;
        }

        //TODO REVISAR SI LA URL AL FIN SE CONCATENA CON EL PATH
        String url = proveedorSelec.getUrl() + "/categoria"
                + "?origen=" + proveedorSelec.getCodigo_empresa()
                + "&cliente=" + proveedorSelec.getCodigo();

        ProgressView.getInstance().Show(() -> {
        });

        Networking.get(url, new Networking.ICallback() {
            @Override
            public void onFail(String error) {
                ProgressView.getInstance().Dismiss();
                Util.showDialog(CatalogoActivity.this, "Alerta", "No se pudo descargar la lista de productos", "Aceptar", "", true, false, failCallDialog, null);
            }

            @Override
            public void onSuccess(String response) {

                ProgressView.getInstance().Dismiss();
                try {

                    JSONArray jsonArray = new JSONArray(response);
                    Type arrayType = new TypeToken<ArrayList<Categoria>>() {
                    }.getType();
                    listaCategorias = new Gson().fromJson(jsonArray.toString(), arrayType);
                    DataController.getInstance().setListaCategoria(listaCategorias);
                } catch (JSONException e) {
                    Util.showDialog(CatalogoActivity.this, "Alerta", "No se pudo descargar la lista de productos", "Aceptar", "", true, false, failCallDialog, null);
                    e.printStackTrace();
                }

            }
        });
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.ultimos_pedidos_menu:
                Intent intent = new Intent(CatalogoActivity.this, UltimoPedidoActivity.class);
                startActivity(intent);
                break;

            case R.id.cartera_menu:
                intent = new Intent(CatalogoActivity.this, CarteraActivity.class);
                startActivity(intent);
                break;

            case R.id.historial_menu:
                intent = new Intent(CatalogoActivity.this, HistorialPedidosActivity.class);
                startActivity(intent);
                break;
            case R.id.salir_menu:

                onBackPressed();
                break;

        }
        return false;
    }

    private void toggleDrawer() {
        if (drawerLayout.isDrawerOpen(Gravity.LEFT)) {
            drawerLayout.closeDrawer(Gravity.LEFT);
        } else {
            drawerLayout.openDrawer(Gravity.LEFT);
        }
    }


    public void mostrarCantidad(int cantidad) {
        if (cantidad == 0) {
            BottomMenuHelper.removeBadge(bottomNavigationView, R.id.navigation_carrito);
        } else {
            BottomMenuHelper.showBadge(this, bottomNavigationView, R.id.navigation_carrito, cantidad + "");
        }
    }


    public Toolbar getToolbar() {

        return toolbar;
    }
}
