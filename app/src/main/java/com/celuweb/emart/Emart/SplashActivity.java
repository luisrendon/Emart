package com.celuweb.emart.Emart;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.celuweb.emart.R;

import java.util.ArrayList;
import java.util.List;

public class SplashActivity extends AppCompatActivity {

    private static int SPLASH_TIME_OUT = 4000;
    private Intent intent = null;
    //Tiempo de duracion de la pantalla de bienvenida: 2.2 Segundos

    private final int RESP_REQUEST_PERMISSION = 100;
    boolean primerVez = true;
    boolean wait = true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
    }


    @Override
    protected void onResume() {
        super.onResume();

        if (primerVez) {
            primerVez = false;
            wait = true;

            if (android.os.Build.VERSION.SDK_INT >= 23) {
                checkPermissions();
            } else {
                startLogin(true);
            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == RESP_REQUEST_PERMISSION) {
            int countPermissionsGranted = 0;

            for (String permission : permissions) {
                int result = ContextCompat.checkSelfPermission(this, permission);
                if (result == PackageManager.PERMISSION_DENIED) {
                    break;
                } else if (result == PackageManager.PERMISSION_GRANTED) {
                    countPermissionsGranted++;
                }
            }

            if (countPermissionsGranted == permissions.length) {
                startLogin(false);
            } else {
                //Faltaron Permisos por Conceder
                new AlertDialog.Builder(this)
                        .setTitle("Permisos Requeridos")
                        .setMessage("Por favor Establezca los permisos para usar la aplicación.")
                        .setCancelable(false)
                        .setPositiveButton("Aceptar", (DialogInterface dialog, int which) -> {
                            if (android.os.Build.VERSION.SDK_INT >= 23) {
                                wait = false;
                                checkPermissions();
                            } else {
                                startLogin(false);
                            }
                        })
                        .show();
            }
        }
    }

    private void checkPermissions() {
        String[] permissions = new String[]{
                Manifest.permission.INTERNET,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA,

        };

        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String permission : permissions) {
            result = ContextCompat.checkSelfPermission(this, permission);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(permission);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            String[] listPermissions = listPermissionsNeeded.toArray(new String[0]);
            ActivityCompat.requestPermissions(this, listPermissions, RESP_REQUEST_PERMISSION);
            wait = false;
        } else {
            startLogin(wait);
        }
    }


    private void startLogin(boolean wait) {

        //intent = new Intent(SplashActivity.this, MainActivity.class);
        //intent = new Intent(SplashActivity.this, CatalogoActivity.class);
        intent = new Intent(SplashActivity.this, LoginActivity.class);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(intent);
                finish();
            }
        }, SPLASH_TIME_OUT);
    }


}
