package com.celuweb.emart.Emart;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.widget.Toolbar;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import androidx.appcompat.app.ActionBar;

import com.celuweb.emart.Adapter.CarteraAdapter;
import com.celuweb.emart.DataController.DataController;
import com.celuweb.emart.DataObject.Cartera;
import com.celuweb.emart.DataObject.Proveedor;
import com.celuweb.emart.Networking.APIs;
import com.celuweb.emart.Networking.Networking;
import com.celuweb.emart.R;
import com.celuweb.emart.Util.ProgressView;
import com.celuweb.emart.Util.ProgressView2;
import com.celuweb.emart.Util.Util;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.concurrent.Callable;

public class CarteraActivity extends Activity {
    ArrayList<Cartera> cartera;
    private RecyclerView recyclerView;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cartera_lista);
        initUI();
        consultarCartera();
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    public void consultarCartera() {
        if (!Util.checkInternet(CarteraActivity.this)) {
            Util.showDialog(CarteraActivity.this, "Alerta", "No Hay conexión a internet", "Aceptar", "", true, false, null, null);
            ProgressView.getInstance().Dismiss();
            return;
        }
        Proveedor proveedorSelect = DataController.getInstance().getProveedor();
        String params = "?origen=" + proveedorSelect.getCodigo_empresa() + "&cliente=" + proveedorSelect.getCodigo();
        String url = Util.getUrl(params, proveedorSelect, APIs.CARTERA_END);
        /*url = "http://66.33.94.211/api/cartera?origen=luker&cliente=2000349861";*/


        ProgressView2.show(this);
        Networking.get(url, new Networking.ICallback() {
            @Override
            public void onFail(String error) {
                ProgressView2.dismiss();
                Util.showDialog(CarteraActivity.this, "Alerta", "No se pudo descargar la informacion de la Cartera", "Aceptar", "", true, false, failCallDialog, null);
            }

            @Override
            public void onSuccess(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    Type arrayType = new TypeToken<ArrayList<Cartera>>() {
                    }.getType();
                    cartera = new Gson().fromJson(jsonArray.toString(), arrayType);
                    ProgressView2.dismiss();
                    cargarRecyclerView();
                } catch (JSONException e) {
                    Util.showDialog(CarteraActivity.this, "Alerta", "No se pudo descargar la informacion de la Cartera", "Aceptar", "", true, false, failCallDialog, null);
                    e.printStackTrace();
                }

            }
        });
    }


    Callable<Void> failCallDialog = new Callable<Void>() {
        @Override
        public Void call() throws Exception {
            onBackPressed();
            return null;
        }
    };


    private void initUI() {
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(CarteraActivity.this, LinearLayoutManager.VERTICAL, false));

    }

    private void cargarRecyclerView() {
        CarteraAdapter adapter = new CarteraAdapter(cartera);
//        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

    }


}
