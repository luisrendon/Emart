package com.celuweb.emart.Util;

import android.app.ProgressDialog;
import android.content.Context;

public class ProgressView2
{
    static ProgressDialog progressDialog;

    public static void show(Context context)
    {
        dismiss();

        progressDialog = new ProgressDialog(context);
        //progressDialog.setTitle("Espere un momento");
        progressDialog.setMessage("Cargando...");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public static void dismiss()
    {
        if(progressDialog != null)
            progressDialog.dismiss();
    }
}

