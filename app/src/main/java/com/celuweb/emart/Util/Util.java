package com.celuweb.emart.Util;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;

import com.celuweb.emart.DataObject.Proveedor;
import com.celuweb.emart.R;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Vector;
import java.util.concurrent.Callable;

public class Util {

    public static void showDialog(Context context, String title, String mensaje, String positiveButtonText, String negativeButtonText, boolean botonSi, boolean botonNo, final Callable<Void> funcionSi, final Callable<Void> funcionNo) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setTitle("Alerta");
        builder.setMessage(mensaje);
        builder.setIcon(android.R.drawable.ic_menu_close_clear_cancel);
        builder.setCancelable(true);

        if (title != null && title != "")
            builder.setTitle(title);

        String positiveText = "Aceptar";
        if (positiveButtonText != null && positiveButtonText != "")
            positiveText = positiveButtonText;

        String negativeText = "Cancelar";
        if (negativeButtonText != null && negativeButtonText != "")
            negativeText = negativeButtonText;

        if (botonSi) {
            builder.setPositiveButton(
                    positiveText,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            try {
                                funcionSi.call();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            dialog.cancel();
                        }
                    });
        }

        if (botonNo) {
            builder.setNegativeButton(
                    negativeText,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            try {
                                funcionNo.call();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            dialog.cancel();
                        }
                    });
        }

        AlertDialog alert11 = builder.create();
        alert11.show();
    }


    /**
     * SOLICITAR PERMISOS
     */
    public static void permisosApp(Activity activity) {
        int PERMISSION_ALL = 1;
        String[] PERMISSIONS = {
                Manifest.permission.INTERNET, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};

        if (!hasPermissions(activity, PERMISSIONS)) {
            ActivityCompat.requestPermissions(activity, PERMISSIONS, PERMISSION_ALL);
        }
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean checkInternet(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        return isConnected;
    }

    public static int toInt(String value) {

        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    public static String toMoney(Float value) {
        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.getDefault());
        format.setMaximumFractionDigits(0);
        String currency = format.format(value);
        return currency;
    }

    public static String GenerarCodigo() {
        String codigo = new SimpleDateFormat("yyyyMMddHHmmssSSS").format(Calendar.getInstance().getTime());
        return codigo;
    }

    public static String GenerarFechaMovil() {
        String codigo = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(Calendar.getInstance().getTime());
        return codigo;
    }

    public static void Headers7(TableLayout tabla, String[] cabecera, Context context) {
        TableRow fila = new TableRow(context);
        for (int i = 0; i < cabecera.length; i++) {
            TextView lbl = new TextView(context);
            lbl.setText("" + cabecera[i]);
            lbl.setPadding(5, 0, 5, 0);
            lbl.setTextColor(Color.argb(255, 255, 255, 255));
            lbl.setBackground(context.getResources().getDrawable(R.drawable.table_cell_row_18));
            fila.addView(lbl);
        }
        tabla.addView(fila);
    }

    public static String SepararMiles(String numero) {
        String cantidad;
        String cantidadAux1;
        String cantidadAux2;
        boolean tieneMenos;
        int posPunto;
        int i;
        cantidad = "";
        cantidadAux1 = "";
        cantidadAux2 = "";
        tieneMenos = false;
        if (numero.indexOf("-") != -1) {
            String aux;
            tieneMenos = true;
            aux = numero.substring(0, numero.indexOf("-"));
            aux = aux + numero.substring(numero.indexOf("-") + 1, numero.length());
            numero = aux;
        }
        if (numero.indexOf(".") == -1) {
            if (numero.length() > 3) {
                cantidad = ColocarComas(numero, numero.length());

            } else {
                if (tieneMenos) numero = "$-" + numero;
                else numero = "$" + numero;
                return numero;
            }

        } else {
            posPunto = numero.indexOf(".");
            for (i = 0; i < posPunto; i++) {
                cantidadAux1 = cantidadAux1 + numero.charAt(i);
            }
            for (i = posPunto; i < numero.length(); i++) {
                cantidadAux2 = cantidadAux2 + numero.charAt(i);
            }
            if (cantidadAux1.length() > 3) {
                cantidad = ColocarComas(cantidadAux1, posPunto);
                cantidad = cantidad + cantidadAux2;

            } else {
                if (tieneMenos) numero = "$-" + numero;
                else numero = "$" + numero;
                return numero;
            }
        }
        if (tieneMenos) cantidad = "$-" + cantidad;
        else cantidad = "$" + cantidad;
        return cantidad;
    }

    private static String ColocarComas(String numero, int pos) {
        String cantidad;
        Vector<String> cantidadAux;
        String cantidadAux1;
        int i;
        int cont;
        cantidadAux = new Vector<String>();
        cantidad = "";
        cantidadAux1 = "";
        cont = 0;
        for (i = (pos - 1); i >= 0; i--) {
            if (cont == 3) {
                cantidadAux1 = "," + cantidadAux1;
                cantidadAux.addElement(cantidadAux1);
                cantidadAux1 = "";
                cont = 0;
            }
            cantidadAux1 = numero.charAt(i) + cantidadAux1;
            cont++;
        }
        cantidad = cantidadAux1;
        for (i = cantidadAux.size() - 1; i >= 0; i--) {
            cantidad = cantidad + cantidadAux.elementAt(i).toString();
        }
        return cantidad;
    }

    public static String getFormatCurrency(Double value) {
        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.US);
        format.setMinimumFractionDigits(0);
        String result = format.format(value);
        result = "$" + result.replaceAll("[^0123456789.,-]", "");
        return result;
    }


    public static String getUrl(String params, Proveedor proveedorSelec, String endpoint) {
        String urlBase = proveedorSelec.getUrl() + proveedorSelec.getPath();
        //TODO CAMBIAR .equasl POR TextUtils.ItsEmpy
        if (TextUtils.isEmpty(urlBase)) {
            urlBase = "http://66.33.94.211/";
        }

       /* String urlBase = proveedorSelec.getUrl() + proveedorSelec.getPath() + TextUtils.isEmpty(proveedorSelec.getPath()) ? "" : "/";*/

         urlBase = proveedorSelec.getUrl() + proveedorSelec.getPath() + (TextUtils.isEmpty(proveedorSelec.getPath()) ? "" : "/");


        return urlBase + endpoint + params;



    }


    public static String round(double d, int decimales){
        String formato = "";
        for (int i = 1; i <= decimales; i++) {
            formato += "0";
        }
        if(formato.length() > 0){
            formato = "#0." + formato;
        }
        else {
            formato = "##.00";
        }
        DecimalFormat df = new DecimalFormat(formato);
        BigDecimal bd = new BigDecimal(Double.toString(d));
        bd = bd.setScale(decimales, BigDecimal.ROUND_HALF_UP);
        return df.format(bd.doubleValue()).replace(",", ".");
    }
}
