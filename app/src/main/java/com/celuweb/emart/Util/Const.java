package com.celuweb.emart.Util;

public class Const {
    //Constantes para el menú del catálogo
    public static final int CATALOGO_FRAGMENT = 1;
    public static final int LISTA_FRAGMENT = 2;
    public static final int CARRITO_FRAGMENT = 3;
    public static final int SUGERIDO_FRAGMENT = 4;
    public static final int BUSCAR_PRODUCTO = 5;


    //Constantes para putExtra
    public static final String SUCURSALES = "SUCURSALES";


    //Constatnes para activityforresults
    public static final int SUCURSALES_ACTIVITY = 1;
}
