package com.celuweb.emart.Networking;

public class APIs {

    public static String baseUrl = "https://emartwebapi.celuwebdev.com/api/";
    public static String baserUrlImgs = "";


    public static String PROVEEDORES = baseUrl.concat("empresa/sucursal?");
    //public static String PROVEEDORES = baseUrl.concat("empresa?");
    public static String VALIDAR = baseUrl + "cliente/validar";
    public static String ACTIVAR = baseUrl + "cliente/activar";
    public static String ENVIOSMS = baseUrl + "cliente/enviarcodigo";

    public static String PEDIDO = baseUrl + "Pedido";
    public static String SUGERIDO = baseUrl + "Sugerido";
    public static String BUSCAR_PRODUCTO = baseUrl + "producto";


    public static String CATEGORIA_END = "api/categoria";
    public static String BUSCAR_END = "api/producto";
    public static String SUGERIDO_END = "api/sugerido";
    public static String PEDIDO_END = "api/pedido";
    public static String REALIZADO_END = "api/pedido/historial";
    public static String CARTERA_END = "api/cartera";

    public static String getImgUrl(String codigoProveedor, String codigoProducto) {
        return baserUrlImgs.concat(codigoProducto).concat(".jpg");
    }


}
