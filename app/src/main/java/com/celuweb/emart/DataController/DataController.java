package com.celuweb.emart.DataController;


import com.celuweb.emart.DataObject.Empresa;
import com.celuweb.emart.DataObject.Pedido;
import com.celuweb.emart.DataObject.PedidoDetalle;
import com.celuweb.emart.DataObject.Categoria;
import com.celuweb.emart.DataObject.Producto;
import com.celuweb.emart.DataObject.Proveedor;
import com.celuweb.emart.DataObject.Sucursal;

import java.util.ArrayList;

public class DataController {
    public static volatile DataController instance;
    private Proveedor proveedor;
    private ArrayList<Proveedor> listaProveedores;
    private ArrayList<Producto> listaProductos;
    private ArrayList<PedidoDetalle> listaPedido;
    private ArrayList<Categoria> listaCategoria;
    private ArrayList<Pedido> pedidosDia = new ArrayList<>();
    private ArrayList<Empresa> listaEmpresas = new ArrayList<>();
    private ArrayList<Sucursal> surcursalesSelec = new ArrayList<>();
    private Empresa empresaSlect= new Empresa();
    private String Codigo;

    private DataController() {
        if (instance != null) {
            throw new RuntimeException("Use getInstance() method to get the single instance of this class.");
        }
    }


    public static DataController getInstance() {
        if (instance == null) { //if there is no instance available... create new one
            synchronized (DataController.class) {
                if (instance == null) instance = new DataController();
            }
        }

        return instance;
    }


    public Proveedor getProveedor() {
        return proveedor;
    }

    public void setProveedor(Proveedor proveedor) {
        this.proveedor = proveedor;
    }

    public String getCodigo() {
        return Codigo;
    }

    public void setCodigo(String codigo) {
        Codigo = codigo;
    }

    public ArrayList<Proveedor> getListaProveedores() {
        return listaProveedores;
    }

    public void setListaProveedores(ArrayList<Proveedor> listaProveedores) {

        if (listaProveedores == null)
            listaProveedores = new ArrayList<>();

        this.listaProveedores = listaProveedores;
    }


    public static void setInstance(DataController instance) {
        DataController.instance = instance;
    }

    public ArrayList<Producto> getListaProductos() {
        return listaProductos;
    }

    public void setListaProductos(ArrayList<Producto> listaProductos) {
        this.listaProductos = new ArrayList<>();
        this.listaProductos = listaProductos;
    }


    public ArrayList<PedidoDetalle> getListaPedido() {
        return listaPedido;
    }

    public void setListaPedido(ArrayList<PedidoDetalle> listaPedido) {
        this.listaPedido = listaPedido;
    }

    public ArrayList<Categoria> getListaCategoria() {
        return listaCategoria;
    }

    public ArrayList<Pedido> getPedidosDia() {
        return pedidosDia;
    }

    public void setPedidosDia(ArrayList<Pedido> pedidosDia) {
        this.pedidosDia = pedidosDia;
    }

    public void setListaCategoria(ArrayList<Categoria> listaCategoria) {
        this.listaCategoria = new ArrayList<>();
        this.listaCategoria = listaCategoria;

    }


    public ArrayList<Empresa> getListaEmpresas() {
        return listaEmpresas;
    }

    public void setListaEmpresas(ArrayList<Empresa> listaEmpresas) {
        this.listaEmpresas = listaEmpresas;
    }

    public ArrayList<Sucursal> getSurcursalesSelec() {
        return surcursalesSelec;
    }

    public void setSurcursalesSelec(ArrayList<Sucursal> surcursalesSelec) {
        this.surcursalesSelec = surcursalesSelec;
    }

    public Empresa getEmpresaSlect() {
        return empresaSlect;
    }

    public void setEmpresaSlect(Empresa empresaSlect) {
        this.empresaSlect = empresaSlect;
    }
}
