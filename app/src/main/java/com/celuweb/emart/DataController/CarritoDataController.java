package com.celuweb.emart.DataController;

import android.util.Log;

import androidx.core.content.res.TypedArrayUtils;

import com.celuweb.emart.DataObject.ListaDetalle;
import com.celuweb.emart.DataObject.Pedido;
import com.celuweb.emart.DataObject.Producto;
import com.celuweb.emart.Util.Util;

import java.util.ArrayList;

public class CarritoDataController {

    public static volatile CarritoDataController instance;

    public ArrayList<Producto> productos = new ArrayList<>();

    private CarritoDataController() {
        if (instance != null) {
            throw new RuntimeException("Use getInstance() method to get the single instance of this class.");
        }
    }

    public static CarritoDataController getInstance() {
        if (instance == null) { //if there is no instance available... create new one
            synchronized (DataController.class) {
                if (instance == null) instance = new CarritoDataController();
            }
        }
        return instance;
    }

    public void addProducto(Producto producto) {
        Log.d("PRODUCTO ENTRANTE", producto.getNombre() + " cantidad: " + producto.getCantidad());

        if (verificarExistenciaProducto(producto)) {
            updateProducto(producto);
        } else {
            productos.add(producto);
        }

        for (Producto p : productos) {
            Log.d("LISTA", p.getNombre() + " cantidad: " + p.getCantidad());
        }
    }

    public void updateProducto(Producto producto) {
        for (Producto p : productos) {
            if (p.getCodigo().equals(producto.getCodigo())) {
                p.setCantidad(producto.getCantidad());
                return;
            }
        }
    }

    public void removeProducto(Producto producto) {
        if (productos.size() > 0) {
            for (Producto p : productos) {
                if (p.getCodigo().equals(producto.getCodigo())) {
                    productos.remove(p);
                    return;
                }
            }
        }
    }

    public Boolean verificarExistenciaProducto(Producto producto) {
        if (productos.size() > 0) {
            for (Producto p : productos) {
                if (p.getCodigo().equals(producto.getCodigo())) {
                    return true;
                }
            }
        }

        return false;
    }

    public int getCantidadProducto(Producto producto) {
        if (productos.size() > 0) {
            for (Producto p : productos) {
                if (p.getCodigo().equals(producto.getCodigo())) {
                    return p.getCantidad();
                }
            }
        }
        return 0;
    }

    public int getCantidadCarrito() {
        int cantidad = 0;
        if (productos.size() > 0) {
            for (Producto p : productos) {
                cantidad += p.getCantidad();
            }
        }
        return cantidad;
    }

    public void borrarCarrito() {
        productos = new ArrayList<>();
    }

    public Pedido generarPedido() {
        ArrayList<ListaDetalle> listaDetalles = new ArrayList<>();
        String numeroDoc = Util.GenerarCodigo();

        for (int i = 0; i < productos.size(); i++) {
            String codigoCliente = DataController.getInstance().getProveedor().getCodigo();
            String codigoProveedor = DataController.getInstance().getProveedor().getNum_empresa() + "";
            String fechaMovil = Util.GenerarFechaMovil();
            int posicion = i;
            int cantidad = productos.get(i).getCantidad();
            String codigoProducto = productos.get(i).getCodigo();
            long precio = Long.parseLong(productos.get(i).getPrecio());
            int iva = productos.get(i).getIva();
            ListaDetalle listaDetalle = new ListaDetalle(numeroDoc, codigoCliente, codigoProveedor, fechaMovil, posicion, cantidad, codigoProducto, precio, iva);
            listaDetalles.add(listaDetalle);
        }

        Pedido pedido = new Pedido(listaDetalles);
        return pedido;
    }
}
